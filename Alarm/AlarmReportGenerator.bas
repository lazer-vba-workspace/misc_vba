Attribute VB_Name = "AlarmReportGenerator"
Sub a_0_generate_report()
    Call a_1_GetAlarmDataAndCreateTables
    Call a_2_delete_empty_rows_from_dataset
    Call a_3_Generate_Alarm_Overview_both
    Call a_4_Generate_LC_Overview
    Call a_5_Format_and_style_alarm_count_and_tech_unav_sheets
    Call a_6_create_lc_top10_alarm_dist_on_park_and_turbine_level_sheets
    Call a_7_format_data_as_Tables_on_overview_pages_and_do_headlines_on_all_sheets
End Sub
'first function who should be called
'remember to select the platform you need data from
Sub a_1_GetAlarmDataAndCreateTables()
    Call Step_0_OpenAllAlarmDataFileAndCopySheet
    
    Sheets("Sheet1").Select
    
    Call Step_1_CreateConnectionAndCreatePivotTable
    'Results in data form 6.0-154 turbine, so if needed for other "uncomment" Step_4_... with the right platform
    Call Step_2_b_StandardSetupPivotFiltersForAlarmReport
    
    
    Call Step_3_DatePeriodFilter_h_Aug20_Jul21
    
    'SELECT RIGHT TURBINE
    'Call Step_4_SetPlatformType_6_154
    'Call Step_4_SetPlatformType_7_154
    'Call Step_4_SetPlatformType_8_154
    'Call Step_4_SetPlatformType_8_167
    'Call Step_4_SetPlatformType_10_193
    
    'the below are called if you want a table with both alarm count and tech unavalibility
        'Call Step_5_a_CopyAlarmPivotResultToNewSheetAndAddFilters_alarmCountTechUnav
    'if the alarm owner, group sub group sheet is added the below can be run right away
        'Call Find_Alarm_Data
        'Call DeleteEntireContentInRow(14)
        'Call DeleteColumnBySearchWord("Total Alarm Count")
       ' Call DeleteColumnBySearchWord("Total Technical Unavailability - Alarm (%)")
    
    Sheets("Sheet1").Select
    
    'get sheet with alarm count data
    Call Filter_RemoveTechUnavaFilter
    Call Step_5_b_CopyAlarmPivotResultToNewSheetAndAddFiltersForAlarmCount
    Call AddSparkLineToCurrentSheet
    'if the alarm owner, group sub group sheet is added the below can be run right away
    Call Find_Alarm_Data
    
    Call DeleteEntireContentInRow(14)
    Call DeleteColumnBySearchWord("grand total")
    
    Sheets("Sheet1").Select
    
    'Get sheet with tech unav data
    Call Filter_AddTechUnavFilter
    Call Filter_RemoveAlarmCountFilter
    Call Step_5_b_CopyAlarmPivotResultToNewSheetAndAddFiltersForAlarmCount
    Call AddSparkLineToCurrentSheet
    'Set 3 decimals on range of tech unav
    Call Set_3_Decimals_on_range("G" & Cells(8, "B") & ":R" & Cells(9, "B"))
    'if the alarm owner, group sub group sheet is added the below can be run right away
    Call Find_Alarm_Data
    
    Call DeleteEntireContentInRow(14)
    Call DeleteColumnBySearchWord("grand total")
    
    'Sheet name for tech unav = Sheet6 (Standard)
    Dim tech_unav_sheet_name As String
    Dim alarm_count_sheet_name As String
    tech_unav_sheet_name = "Sheet6"
    alarm_count_sheet_name = "Sheet5"
    'Sheet name for alarm count = Sheet5 (Standard)
    'Needs to be updated if names have changed
    Call GetReportedTurbines(tech_unav_sheet_name, alarm_count_sheet_name)
    'Reformat the way the months are displayed
    Call ChangeMonthFormatInDataSheets(tech_unav_sheet_name)
    Call ChangeMonthFormatInDataSheets(alarm_count_sheet_name)
    
     
End Sub
'delete all wtc3 alarms from dataset
'this function can be called if we have mistakenly recived wtc3 alarms from polaris once we created the inital
'alarm count and tech unav sheets
Function a_0b_Delete_WTC3_Alarms_From_DataSet()

    'go to the count sheet
    Sheets("Sheet5").Select
    Call del_wtc3_alarms_convert_entire_alarm_range_from_text_to_number_and_sort
    Call del_wtc3_alarms_check_if_cell_have_below_6_digits_if_not_delete_row
    'update end row number for the alarm range, since they are used in other functions
    Cells("9", "B") = LastRowInOneColumn("a")
    
    'go to the tech unav sheet
    Sheets("Sheet6").Select
    Call del_wtc3_alarms_convert_entire_alarm_range_from_text_to_number_and_sort
    Call del_wtc3_alarms_check_if_cell_have_below_6_digits_if_not_delete_row
    'update end row number for the alarm range, since they are used in other functions
    Cells("9", "B") = LastRowInOneColumn("a")
End Function

Sub a_2_delete_empty_rows_from_dataset()
    'loop this thorugh 25 times so we now we have all empty rows removed
    'we do this since there is a bug in the Delete_empty_Rows_from_data_set()
    For i = 1 To 25
        'go to the count sheet
        Sheets("Sheet5").Select
        Call Delete_empty_Rows_from_data_set
        
        'go to the tech unav sheet
        Sheets("Sheet6").Select
        Call Delete_empty_Rows_from_data_set
        Range("A1").Select
    Next i

    
End Sub


'takes both for alarm count and tech unavability
'remember to change the variables in the function to match the correct sheet name and end row
Sub a_3_Generate_Alarm_Overview_both()
    Call c_step_0_AlarmOverviewBasicSettings
    Call c_step_1_CreateAlarmOverview
    'remember to add sheet name here for the alarm count sheet
    'sheet name for alarm count sheet
    Cells(3, "B").Value = "Sheet5"
    'last month column
    Cells(13, "B").Value = "R"
    
    Dim a_sheetname As String
    a_sheetname = Cells(3, "B")
    Dim b_sheetname As String
    
    'tech unava
    'sheet name for technical unavability
    b_sheetname = "Sheet6"
    
    'start end row variables
    Dim ownerStartRow As Integer
    Dim ownerEndRow As Integer
    Dim groupStartRow As Integer
    Dim groupEndRow As Integer

    
    'Alarms stats By Technical Unavailability and alarm count All Owners and group/sub group
    
    ownerStartRow = 19
    ownerEndRow = 37
    groupStartRow = 40
    groupEndRow = 87
    
    
    
    'SUM of alarms by owner
    Call c_step_x2x_GetSumOfAlarmsDataByOwner(ownerStartRow, ownerEndRow, a_sheetname, "B")
    'SUM of tech unav by owner
    Call c_step_x2x_GetSumOfAlarmsDataByOwner(ownerStartRow, ownerEndRow, b_sheetname, "C")
    'Call Set_3_Decimals_on_range("C" & ownerStartRow & ":C" & Cells(9, "B"))
    'SUM of count by group/sub group
    Call c_step_x2x_GetSumOfAlarmsDataByGroup(groupStartRow, groupEndRow, a_sheetname, "C")
    'SUM of tech unav by group/sub grop
    Call c_step_x2x_GetSumOfAlarmsDataByGroup(groupStartRow, groupEndRow, b_sheetname, "D")
    'change copied cells for tech unav to show percentage instead of odd numbers
    Call ChangeRangeToPercentage("C" & ownerStartRow & ":C" & ownerEndRow)
    Call ChangeRangeToPercentage("D" & groupStartRow & ":D" & groupEndRow)
    
    
    'top 10 tech unav and alarm count all owners
    
    Dim top_10_tech_unav_start_row As Integer
    Dim top_10_tech_unav_end_row As Integer
    Dim top_10_alarm_count_start_row As Integer
    Dim top_10_alarm_count_end_row As Integer
    
    top_10_tech_unav_start_row = 90
    top_10_tech_unav_end_row = 102
    top_10_alarm_count_start_row = 106
    top_10_alarm_count_end_row = 120
    
    'Create top 10 lists for tech unav and alarm count
    Call c_step_4_Get_top_10_by_alarm_tech_unav(top_10_tech_unav_start_row, top_10_tech_unav_end_row, "sheet6")
    Call c_step_4_Get_top_10_by_alarm_count(top_10_alarm_count_start_row, top_10_alarm_count_end_row, "Sheet5")
    'style document
    Call c_AlarmOverview_create_final_Headlines
    
    
End Sub

'Create a overview over L&C related alarms
'to do list are added in the function
Sub a_4_Generate_LC_Overview()
    'create new sheet
    Sheets.Add After:=ActiveSheet
    
    Dim LC_view_sheet_name As String
    Dim tech_unav_sheet_name As String
    Dim alarm_count_sheet_name As String
    Dim alarm_overview_sheet_name As String
    
    LC_view_sheet_name = ActiveSheet.Name
    tech_unav_sheet_name = "Sheet6"
    alarm_count_sheet_name = "Sheet5"
    alarm_overview_sheet_name = "Sheet7"
    '"Sheet7"
    
    Cells(6, "A") = "Alarm Report " & Worksheets("Sheet1").Range("E1")
    Cells(8, "A") = "L&C View"
    
    Cells(12, "A") = "Total L&C Triggered Alarms " & Worksheets(alarm_count_sheet_name).Range("R15")
    Cells(12, "D") = Worksheets(alarm_overview_sheet_name).Range("B24") + Worksheets(alarm_overview_sheet_name).Range("B30") + Worksheets(alarm_overview_sheet_name).Range("B33")
    
    Cells(13, "A") = "Total L&C Technical Unavailability " & Worksheets(alarm_count_sheet_name).Range("R15")
    Cells(13, "D") = Worksheets(alarm_overview_sheet_name).Range("C24") + Worksheets(alarm_overview_sheet_name).Range("C30") + Worksheets(alarm_overview_sheet_name).Range("C33")
    Range("D13").Select
    Selection.NumberFormat = "0.000%"
    
    Cells(18, "A") = "Top L&C Alarms For " & Worksheets(tech_unav_sheet_name).Range("R15")
    
    'set the headers for the tech unav
    Cells(24, "A") = "By Technical Unavailability"
    Cells(24, "E") = "Owner Groups: SystemLevelControl, TurbineControl,Environment"
    'copy top 10 L&C tech unav alarms from the tech unav sheet along with alarm code, description and so on
    Call d_Get_Top_10_LC_Alarms(tech_unav_sheet_name, "A15", "a", LC_view_sheet_name, "a25")
    Call d_Get_Top_10_LC_Alarms(tech_unav_sheet_name, "B15", "b", LC_view_sheet_name, "b25")
    Call d_Get_Top_10_LC_Alarms(tech_unav_sheet_name, "C15", "c", LC_view_sheet_name, "c25")
    Call d_Get_Top_10_LC_Alarms(tech_unav_sheet_name, "D15", "d", LC_view_sheet_name, "d25")
    Call d_Get_Top_10_LC_Alarms(tech_unav_sheet_name, "E15", "e", LC_view_sheet_name, "e25")
    Call d_Get_Top_10_LC_Alarms(tech_unav_sheet_name, "R15", "r", LC_view_sheet_name, "f25")
    
    
    'Set the headers for alarm count
    Cells(41, "A") = "By Alarm Count"
    Cells(41, "E") = "Owner Groups: SystemLevelControl, TurbineControl,Environment"
    'copy top 10 L&C alarm counts from the alarm count sheet along with alarm code, description and so on
    Call d_Get_Top_10_LC_Alarms(alarm_count_sheet_name, "A15", "a", LC_view_sheet_name, "a42")
    Call d_Get_Top_10_LC_Alarms(alarm_count_sheet_name, "B15", "b", LC_view_sheet_name, "b42")
    Call d_Get_Top_10_LC_Alarms(alarm_count_sheet_name, "C15", "c", LC_view_sheet_name, "c42")
    Call d_Get_Top_10_LC_Alarms(alarm_count_sheet_name, "D15", "d", LC_view_sheet_name, "d42")
    Call d_Get_Top_10_LC_Alarms(alarm_count_sheet_name, "E15", "e", LC_view_sheet_name, "e42")
    Call d_Get_Top_10_LC_Alarms(alarm_count_sheet_name, "R15", "r", LC_view_sheet_name, "f42")
    
    'Reset filters on alarm sheets
   
    Sheets(tech_unav_sheet_name).Select
    ActiveSheet.Range("$A$" & Worksheets(tech_unav_sheet_name).Range("B8") - 1 & ":$R$" & Worksheets(tech_unav_sheet_name).Range("B9")).AutoFilter Field:=5
    
    
    Sheets(alarm_count_sheet_name).Select
    ActiveSheet.Range("$A$" & Worksheets(alarm_count_sheet_name).Range("B8") - 1 & ":$R$" & Worksheets(alarm_count_sheet_name).Range("B9")).AutoFilter Field:=5
    
    Sheets(LC_view_sheet_name).Select
    
    'Set the headers for the month by month L&C tech unav
    Cells(65, "A") = "Technical Unavailability Caused By L&C Owned Alarms - Monthly View - Last 12 Months"
    Cells(66, "A") = "Month"
    Cells(67, "A") = "Technical Unavailability"
    Cells(68, "A") = "Reported turbines"
    
    'copy paste the last 12 months text description from the tech unav sheet
    Worksheets(tech_unav_sheet_name).Range("G15:R15").Copy
    Range("B66").Select
    ActiveSheet.Paste
    'get sum of l&C tech unav by the month and insert in the table
    Call d_GetSumOfTechUnavAlarmsDataLConly(tech_unav_sheet_name)
    'copy and paste number of reported turbines from the tech unav sheet
    Worksheets(tech_unav_sheet_name).Range("G14:R14").Copy
    Range("B68").Select
    ActiveSheet.Paste
    
    Call d_LC_view_style_page
    'row 65
    'insert the below test
    '---Top L&C Alarms For Mar 2021
    '---By Technical Unavailability
    '---Owner Groups: SystemLevelControl, TurbineControl,Environment
    '---By Alarm Count
    '---Technical Unavailability Caused By L&C Owned Alarms - Monthly View - Last 12 Months
    '---O
    
    
    'total numbers of triggered L&C alarms
    '----to to alarm overview plus L&c groups (env, tc, slc)
    'total % of tech unav by L&C alarms
    '----to to alarm overview plus L&c groups (env, tc, slc)
    
    'top lists for tech unav
    '.... functions needs to be called here
    'function is created Call d_step_XX_GetSumOfTechUnavAlarmsDataLConly() - might need to be modified a bit
    
    
    'top lists for alarm count
    '.... functions needs to be called here
    
    
    'total monthly tech unav caused by L&C related alarms
    '.... functions needs to be called here
End Sub

'Call this function after formatting the tables on Alarm count + tech unav sheet to add sparklines to the sheets
'Reason for this is after formatting the tables the sparklines are not following the alarm numbers
Sub a_5_Format_and_style_alarm_count_and_tech_unav_sheets()
    'initate formatting of the data on alarm count and tech unav sheets
    '************
    'for the alarm count sheet
    Sheets("Sheet5").Select
    Call format_alarm_count_and_tech_unav_sheets
    
    'for the tech unav sheet
    Sheets("Sheet6").Select
    Call format_alarm_count_and_tech_unav_sheets
    
    'add spark lines, this is needed - since the sparkline will not follow a given alarm when sorting the alarms
    Call ReAddSparkLineAuto("Sheet5")
    Call ReAddSparkLineAuto("Sheet6")
    
    'Style alarm count and tech unav sheets
    Sheets("Sheet5").Select
    Call style_cells_and_row_for_alarmcount_and_tech_unav_sheets
    'Run it for tech unav sheet
    Sheets("Sheet6").Select
    Call style_cells_and_row_for_alarmcount_and_tech_unav_sheets
    
End Sub

Sub a_4_rename_sheets()
    Call RenameSheets
    'Top10LC alarm_site_distribution / Sheet9
    'Top10LC alarm_turb_distribution / Sheet10

End Sub

Sub a_7_format_data_as_Tables_on_overview_pages_and_do_headlines_on_all_sheets()
    Sheets("L&C Alarm Overview").Select
    Call HelperModule.format_range_as_table("a25:f35", 2)
    
    Call HelperModule.format_range_as_table("a42:f52", 2)
    
    Sheets("Alarm Overview Last Month").Select
    'delete unneeded row
    Rows("40:40").Select
    Selection.Delete Shift:=xlUp
    Range("C40").Select
    'rename cells to the right text
    ActiveCell.FormulaR1C1 = "Alarm Count"
    Range("D40").Select
    ActiveCell.FormulaR1C1 = "Technical Unavailability"
    Range("D41").Select
    
    Call HelperModule.format_range_as_table("a18:c37", 2)
    
    Call HelperModule.format_range_as_table("A40:D86", 2)
    
    'remove unwanted background fill in cells - coming from copying it from the raw data
    Rows("89:89").Select
    With Selection.Interior
        .Pattern = xlNone
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    Rows("105:105").Select
    With Selection.Interior
        .Pattern = xlNone
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With

    
    Call HelperModule.format_range_as_table("A89:F99", 2)
    
    Call HelperModule.format_range_as_table("A105:F115", 2)
    
    Call Change_column_width("L&C Alarm Overview", "12.57", "a")
    Call Change_column_width("L&C Alarm Overview", "20", "b")
    Call Change_column_width("L&C Alarm Overview", "11.14", "c")
    Call Change_column_width("L&C Alarm Overview", "20", "d")
    Call Change_column_width("L&C Alarm Overview", "19", "e")
    Call Change_column_width("L&C Alarm Overview", "15", "f")
    
    Call Change_column_width("Alarm Overview Last Month", "23.57", "a")
    Call Change_column_width("Alarm Overview Last Month", "25", "b")
    Call Change_column_width("Alarm Overview Last Month", "19", "c")
    Call Change_column_width("Alarm Overview Last Month", "16.86", "d")
    Call Change_column_width("Alarm Overview Last Month", "18.29", "e")
    Call Change_column_width("Alarm Overview Last Month", "15", "f")
    Call Change_column_width("Alarm Overview Last Month", "29", "g")
    Call Change_column_width("Alarm Overview Last Month", "8.43", "h")
    
    'wrap text in tables on overview pages after the width are changed
    Sheets("L&C Alarm Overview").Select
    HelperModule.WrapText_in_range ("A25:F35")
    HelperModule.WrapText_in_range ("A42:F52")
    'Correct wrong header
    Range("F25") = "Technical Unavailability"
    Range("F42") = "Alarm Count"
    'wrap and align headers showing "Owner Groups: SystemLevelControl, TurbineControl,Environment"
    Range("A12:C12").Select
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    Range("E23:F24").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = True
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = True
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    ActiveWindow.SmallScroll Down:=15
    Range("E40:F41").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = True
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = True
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    
    Sheets("Alarm Overview Last Month").Select
    HelperModule.WrapText_in_range ("A18:C37")
    HelperModule.WrapText_in_range ("A40:D86")
    HelperModule.WrapText_in_range ("A89:F99")
    HelperModule.WrapText_in_range ("A105:F115")
    
    
    'add stamp for when report was generated
    Worksheets("L&C Alarm Overview").Range("F1") = "Author: lars.mathiesen@siemensagamesa.com"
    Worksheets("L&C Alarm Overview").Range("F2") = "Source: Polaris DWH"
    Worksheets("L&C Alarm Overview").Range("F3") = "Report generated: " & Date
    
    Worksheets("Alarm Overview Last Month").Range("F1") = "Author: lars.mathiesen@siemensagamesa.com"
    Worksheets("Alarm Overview Last Month").Range("F2") = "Source: Polaris DWH"
    Worksheets("Alarm Overview Last Month").Range("F3") = "Report generated: " & Date
    
    Call copy_info_boxes_from_old_alarm_report_and_headlines
    
    'move info boxes to right places
    Sheets("L&C Alarm Overview").Select
    ActiveSheet.Shapes.Range(Array("Group 9")).Select
    Selection.ShapeRange.IncrementLeft 4.5
    Selection.ShapeRange.IncrementTop 38.25
    ActiveSheet.Shapes.Range(Array("Group 12")).Select
    Selection.ShapeRange.IncrementLeft 6.75
    Selection.ShapeRange.IncrementTop 155.25
    Selection.ShapeRange.ScaleWidth 1.1607670882, msoFalse, msoScaleFromTopLeft
    Selection.ShapeRange.ScaleHeight 1.6022319715, msoFalse, msoScaleFromTopLeft
    Selection.ShapeRange.IncrementLeft -32.25
    Selection.ShapeRange.IncrementTop -21
    
    'scale SGRE logo
    ActiveSheet.Shapes.Range(Array("Picture 2")).Select
    Selection.ShapeRange.ScaleWidth 1.3579612885, msoFalse, msoScaleFromTopLeft
    Selection.ShapeRange.ScaleHeight 1.3579612885, msoFalse, msoScaleFromTopLeft
    
    'move info boxes to right places
    Sheets("Alarm Overview Last Month").Select
    ActiveSheet.Shapes.Range(Array("Group 3")).Select
    Selection.ShapeRange.IncrementLeft 1.5
    Selection.ShapeRange.IncrementTop -33
    ActiveWindow.SmallScroll Down:=18
    ActiveSheet.Shapes.Range(Array("Group 6")).Select
    Selection.ShapeRange.IncrementLeft 0.75
    Selection.ShapeRange.IncrementTop -22.5
    ActiveWindow.SmallScroll Down:=45
    ActiveSheet.Shapes.Range(Array("Group 9")).Select
    Selection.ShapeRange.IncrementLeft -0.75
    Selection.ShapeRange.IncrementTop -82.5
    ActiveWindow.SmallScroll Down:=21
    ActiveSheet.Shapes.Range(Array("Group 12")).Select
    Selection.ShapeRange.IncrementLeft -15
    Selection.ShapeRange.IncrementTop -85.5
    
    'scale sgre logo
    ActiveSheet.Shapes.Range(Array("Picture 2")).Select
    Selection.ShapeRange.ScaleWidth 1.3579612885, msoFalse, msoScaleFromTopLeft
    Selection.ShapeRange.ScaleHeight 1.357961159, msoFalse, msoScaleFromTopLeft
    
    'format and merge cells which needs that
    Rows("38:39").Select
    Range("D38").Activate
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    ActiveWindow.SmallScroll Down:=15
    Range("A40:D41").Select
    With Selection
        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.UnMerge
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    ActiveWindow.SmallScroll Down:=48
    Rows("89:90").Select
    Range("D89").Activate
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("A91:F92").Select
    Range("E91").Activate
    With Selection
        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    With Selection
        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.UnMerge
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    
    ActiveSheet.Shapes.Range(Array("Group 6")).Select
    Selection.ShapeRange.IncrementLeft -0.75
    Selection.ShapeRange.IncrementTop -26.25
    ActiveWindow.SmallScroll Down:=36
    ActiveSheet.Shapes.Range(Array("Group 9")).Select
    Selection.ShapeRange.ScaleHeight 0.7689772206, msoFalse, msoScaleFromTopLeft
    Selection.ShapeRange.IncrementLeft 0.75
    Selection.ShapeRange.IncrementTop 31.5
    
    
    Range("A107:F108").Select
    Range("E107").Activate
    With Selection
        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    With Selection
        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.UnMerge
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
End Sub

'copies info boxes from old alarm report on overview pages
'headliens for complete view pages
'formats data as tables on overview pages, and changes the width to be properly and wraps text afterwards
Function copy_info_boxes_from_old_alarm_report_and_headlines()
    'get full file name for current report
    Dim this_file_name As String
    this_file_name = ThisWorkbook.Name
    
    'open old alarm report
    Workbooks.Open Filename:= _
        "https://siemensgamesa.sharepoint.com/teams/OG00570/Product/TurbineAlarms/L&C%20Alarm%20Reports/2021/2021-06/LC-Alarm-Report-06.0-154-2021-06.xlsm"
    'start on LC alarm overview and copy info boxes to new alarm report
    Sheets("L&C Alarm Overview").Select
    Range("F1:F3").Select
    ActiveSheet.Shapes.Range(Array("Group 16")).Select
    ActiveSheet.Shapes.Range(Array("Group 16", "Group 19")).Select
    ActiveSheet.Shapes.Range(Array("Group 16", "Group 19", "Group 22")).Select
    ActiveSheet.Shapes.Range(Array("Group 16", "Group 19", "Group 22", _
        "Group 25")).Select
    Selection.Copy
    
    
    'go back to new file and paste the info boxes
    Windows(this_file_name).Activate
    'Windows("LC-Alarm-Report-06.0-154-2021-06.xlsm").Activate
    Windows(this_file_name).Activate
    Sheets("L&C Alarm Overview").Select
    Range("E6").Select
    ActiveSheet.Paste
    Range("H44").Select
    Sheets("Alarm Overview Last Month").Select
    
    'go to the old alarm report and copy data for alarm overview last month
    Windows("LC-Alarm-Report-06.0-154-2021-06.xlsm").Activate
    Sheets("Alarm Overview Last Month").Select
    ActiveSheet.Shapes.Range(Array("Group 3")).Select
    ActiveSheet.Shapes.Range(Array("Group 3", "Group 6")).Select
    ActiveSheet.Shapes.Range(Array("Group 3", "Group 6", "Group 9")).Select
    ActiveSheet.Shapes.Range(Array("Group 3", "Group 6", "Group 9", "Group 12") _
        ).Select
    Selection.Copy
    
    'go back to the new alarm report
    Windows(this_file_name).Activate
    Range("D16").Select
    ActiveSheet.Paste
    Range("G34").Select
    
    'go back to the old alarm report and copy for tech unav sheet
    Windows("LC-Alarm-Report-06.0-154-2021-06.xlsm").Activate
    Sheets("Technical Unavailability").Select
    Rows("1:13").Select
    Selection.Copy
    
    'go back to the new report and paste headlines and info boxes
    Windows(this_file_name).Activate
    Sheets("Technical Unavailability").Select
    Range("A1").Select
    ActiveSheet.Paste
    Range("B9").Select
    
    'go to the old alarm report and copy headlines for alarm count sheet
    Windows("LC-Alarm-Report-06.0-154-2021-06.xlsm").Activate
    Sheets("Alarm Count").Select
    Rows("1:14").Select
    Application.CutCopyMode = False
    Selection.Copy
    Rows("1:13").Select
    Application.CutCopyMode = False
    Selection.Copy
    
    'go back to the new report and paste on the alarm count sheet
    Windows(this_file_name).Activate
    Sheets("Alarm Count").Select
    Range("A5").Select
    Range("A1").Select
    ActiveSheet.Paste
    Range("D11").Select
    Sheets("Top10LC alarm_site_distribution").Select
    Rows("1:2").Select
    Application.CutCopyMode = False
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    'go back to the old alarm report and copy for top10 lc site dist
    Windows("LC-Alarm-Report-06.0-154-2021-06.xlsm").Activate
    Sheets("Top10LC alarm_site_distribution").Select
    Rows("1:1").Select
    Selection.Copy
    
    'go to the new report and paste
    Windows(this_file_name).Activate
    Range("A1").Select
    ActiveSheet.Paste
    
    'go to the old report and copy for turbine dist
    Windows("LC-Alarm-Report-06.0-154-2021-06.xlsm").Activate
    Sheets("Top10LC alarm_turb_distribution").Select
    Rows("1:1").Select
    Application.CutCopyMode = False
    Selection.Copy
    
    'go to the new report and paste
    Windows(this_file_name).Activate
    Sheets("Top10LC alarm_turb_distribution").Select
    Range("A1").Select
    ActiveSheet.Paste
    'Range("A5").Select
    
    'close the old report
    Windows("LC-Alarm-Report-06.0-154-2021-06.xlsm").Activate
    ActiveWindow.Close
End Function




'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// Open alarm file and copy the sheet to the curernt workbook and close alarm file agin. \\\\\\\\\\\\\\\\\\\'
'                                              START'
'---------------------------------------------------------'
'---------------------------------------------------------'



Sub Step_0_OpenAllAlarmDataFileAndCopySheet()
'
' Macro1 Macro
'

'
    Dim report_file_name As String
    report_file_name = ThisWorkbook.Name
    
    Workbooks.Open Filename:= _
        "https://siemensgamesa.sharepoint.com/teams/OG00570/Product/TurbineAlarms/Alarms_Groups_And_Owners.xlsx"
    Sheets("AllAlarms").Select
    Sheets("AllAlarms").Copy After:=Workbooks(report_file_name).Sheets(1)
    Windows("Alarms_Groups_And_Owners.xlsx").Activate
    Sheets("AlarmOwnerGroups").Select
    Sheets("AlarmOwnerGroups").Copy After:=Workbooks(report_file_name).Sheets(1)
    Windows("Alarms_Groups_And_Owners.xlsx").Activate
    Sheets("All Groups").Select
    Sheets("All Groups").Copy After:=Workbooks(report_file_name).Sheets(1)
    Windows("Alarms_Groups_And_Owners.xlsx").Activate
    ActiveWindow.Close
End Sub

'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// SET Crete Connection to Polaris and create Pivot table \\\\\\\\\\\\\\\\\\\'
'                                              START'
'---------------------------------------------------------'
'---------------------------------------------------------'

Sub Step_1_CreateConnectionAndCreatePivotTable()
'
' Macro2 Macro
'

'

    Dim report_file_name As String
    report_file_name = ThisWorkbook.Name
    
    Application.CutCopyMode = False
    Workbooks(report_file_name).Connections.Add2 _
        "VMPRDSSAS01.prod.sgre.one ExitoV2 Standard", _
        "Pivot connection to Polaris DWH on Azure Cloud", Array( _
        "OLEDB;Provider=MSOLAP.8;Integrated Security=SSPI;Persist Security Info=True;Data Source=VMPRDSSAS01.prod.sgre.one;Update Isolation Lev" _
        , "el=2;Initial Catalog=ExitoV2"), "Standard", 1
    ActiveWorkbook.PivotCaches.Create(SourceType:=xlExternal, SourceData:= _
        ActiveWorkbook.Connections("VMPRDSSAS01.prod.sgre.one ExitoV2 Standard"), _
        Version:=6).CreatePivotTable TableDestination:="Sheet1!R1C1", tableName:= _
        "PivotTable1", DefaultVersion:=6
    Cells(1, 1).Select
    With ActiveSheet.PivotTables("PivotTable1")
        .ColumnGrand = True
        .HasAutoFormat = True
        .DisplayErrorString = False
        .DisplayNullString = True
        .EnableDrilldown = True
        .ErrorString = ""
        .MergeLabels = False
        .NullString = ""
        .PageFieldOrder = 2
        .PageFieldWrapCount = 0
        .PreserveFormatting = True
        .RowGrand = True
        .PrintTitles = False
        .RepeatItemsOnEachPrintedPage = True
        .TotalsAnnotation = True
        .CompactRowIndent = 1
        .VisualTotals = False
        .InGridDropZones = False
        .DisplayFieldCaptions = True
        .DisplayMemberPropertyTooltips = True
        .DisplayContextTooltips = True
        .ShowDrillIndicators = True
        .PrintDrillIndicators = False
        .DisplayEmptyRow = False
        .DisplayEmptyColumn = False
        .AllowMultipleFilters = False
        .SortUsingCustomLists = True
        .DisplayImmediateItems = True
        .ViewCalculatedMembers = True
        .EnableWriteback = False
        .ShowValuesRow = False
        .CalculatedMembersInFilters = True
        .RowAxisLayout xlCompactRow
    End With
    ActiveSheet.PivotTables("PivotTable1").PivotCache.RefreshOnFileOpen = False
    ActiveSheet.PivotTables("PivotTable1").RepeatAllLabels xlRepeatLabels
End Sub

'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// SET Crete Connection to Polaris and create Pivot table \\\\\\\\\\\\\\\\\\\'
'                                              END'
'---------------------------------------------------------'
'---------------------------------------------------------'


'*********************************************************************************************************'
'*********************************************************************************************************'


'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// SET All standard filters for generating a alarm report \\\\\\\\\\\\\\\\\\\'
'                                            START'
'---------------------------------------------------------'
'---------------------------------------------------------'

Sub Step_2_a_StandardSetupPivotFiltersForAlarmReport_alarmCountOnly()
'
' StandardSetup Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").CubeFields("[Turbine].[Is Prototype]"). _
        CreatePivotFields
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[Is Prototype].[Prototype]").VisibleItemsList = Array( _
        "[Turbine].[Is Prototype].[Prototype].&[No]")
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[Is Prototype]")
        .Orientation = xlRowField
        .Position = 1
    End With
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[Is Prototype]")
        .Orientation = xlPageField
        .Position = 1
    End With
    ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[SWT Platform -Type]").CreatePivotFields
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[Swt Platform]").VisibleItemsList = Array("")
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[SwtType]").VisibleItemsList = Array( _
        "[Turbine].[SWT Platform -Type].[Swt Platform].&[D6].&[SWT-6.0-154]")
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[SWT Platform -Type]")
        .Orientation = xlRowField
        .Position = 1
    End With
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[SWT Platform -Type]")
        .Orientation = xlPageField
        .Position = 2
    End With
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Alarm Generic Information].[Alarm Code]")
        .Orientation = xlRowField
        .Position = 1
    End With
    ActiveSheet.PivotTables("PivotTable1").CubeFields("[Time].[Month]"). _
        CreatePivotFields
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2021-01-01T00:00:00]")
    With ActiveSheet.PivotTables("PivotTable1").CubeFields("[Time].[Month]")
        .Orientation = xlColumnField
        .Position = 1
    End With
    ActiveSheet.PivotTables("PivotTable1").AddDataField ActiveSheet.PivotTables( _
        "PivotTable1").CubeFields("[Measures].[Alarm Count]")

        
        
    Cells(1, "H").Value = "A4:AA324"
    Cells(2, "H").Value = "Remember to set table range in Cell H1 before using step 5 ala: A4:AA324"
    Cells(1, "I").Value = "A16:AA16"
    Cells(2, "I").Value = "Assign the length of the columns from Cell H1 ala: A17:Y17"
    
End Sub


Sub Step_2_b_StandardSetupPivotFiltersForAlarmReport()
'
' StandardSetup Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").CubeFields("[Turbine].[Is Prototype]"). _
        CreatePivotFields
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[Is Prototype].[Prototype]").VisibleItemsList = Array( _
        "[Turbine].[Is Prototype].[Prototype].&[No]")
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[Is Prototype]")
        .Orientation = xlRowField
        .Position = 1
    End With
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[Is Prototype]")
        .Orientation = xlPageField
        .Position = 1
    End With
    ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[SWT Platform -Type]").CreatePivotFields
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[Swt Platform]").VisibleItemsList = Array("")
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[SwtType]").VisibleItemsList = Array( _
        "[Turbine].[SWT Platform -Type].[Swt Platform].&[D6].&[SWT-6.0-154]")
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[SWT Platform -Type]")
        .Orientation = xlRowField
        .Position = 1
    End With
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[SWT Platform -Type]")
        .Orientation = xlPageField
        .Position = 2
    End With
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Alarm Generic Information].[Alarm Code]")
        .Orientation = xlRowField
        .Position = 1
    End With
    ActiveSheet.PivotTables("PivotTable1").CubeFields("[Time].[Month]"). _
        CreatePivotFields
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2021-01-01T00:00:00]")
    With ActiveSheet.PivotTables("PivotTable1").CubeFields("[Time].[Month]")
        .Orientation = xlColumnField
        .Position = 1
    End With
    ActiveSheet.PivotTables("PivotTable1").AddDataField ActiveSheet.PivotTables( _
        "PivotTable1").CubeFields("[Measures].[Alarm Count]")
    ActiveSheet.PivotTables("PivotTable1").AddDataField ActiveSheet.PivotTables( _
        "PivotTable1").CubeFields("[Measures].[Technical Unavailability - Alarm (%)]")
        
        
    Cells(1, "H").Value = "A4:AA324"
    Cells(2, "H").Value = "Remember to set table range in Cell H1 before using step 5 ala: A4:AA324"
    Cells(1, "I").Value = "A16:AA16"
    Cells(2, "I").Value = "Assign the length of the columns from Cell H1 ala: A17:Y17"
    
End Sub

'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// SET All standard filters for generating a alarm report \\\\\\\\\\\\\\\\\\\'
'                                            END'
'---------------------------------------------------------'
'---------------------------------------------------------'



'*********************************************************************************************************'
'*********************************************************************************************************'


'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// SET MONTH FILTER \\\\\\\\\\\\\\\\\\\'
'                          START'
'---------------------------------------------------------'
'---------------------------------------------------------'
'Jan20-Dec20'
Sub Step_3_DatePeriodFilter_a_Jan20_Dec20()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-01-01T00:00:00]", _
        "[Time].[Month].&[2020-02-01T00:00:00]", "[Time].[Month].&[2020-03-01T00:00:00]" _
        , "[Time].[Month].&[2020-04-01T00:00:00]", _
        "[Time].[Month].&[2020-05-01T00:00:00]", "[Time].[Month].&[2020-06-01T00:00:00]" _
        , "[Time].[Month].&[2020-07-01T00:00:00]", _
        "[Time].[Month].&[2020-08-01T00:00:00]", "[Time].[Month].&[2020-09-01T00:00:00]" _
        , "[Time].[Month].&[2020-10-01T00:00:00]", _
        "[Time].[Month].&[2020-11-01T00:00:00]", "[Time].[Month].&[2020-12-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'Feb20-Jan21'
Sub Step_3_DatePeriodFilter_b_Feb20_Jan21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-02-01T00:00:00]", _
        "[Time].[Month].&[2020-03-01T00:00:00]", "[Time].[Month].&[2020-04-01T00:00:00]" _
        , "[Time].[Month].&[2020-05-01T00:00:00]", _
        "[Time].[Month].&[2020-06-01T00:00:00]", "[Time].[Month].&[2020-07-01T00:00:00]" _
        , "[Time].[Month].&[2020-08-01T00:00:00]", _
        "[Time].[Month].&[2020-09-01T00:00:00]", "[Time].[Month].&[2020-10-01T00:00:00]" _
        , "[Time].[Month].&[2020-11-01T00:00:00]", _
        "[Time].[Month].&[2020-12-01T00:00:00]", "[Time].[Month].&[2021-01-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub
'Mar20-Feb21'
Sub Step_3_DatePeriodFilter_c_Mar20_Feb21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-03-01T00:00:00]", _
        "[Time].[Month].&[2020-04-01T00:00:00]", "[Time].[Month].&[2020-05-01T00:00:00]" _
        , "[Time].[Month].&[2020-06-01T00:00:00]", _
        "[Time].[Month].&[2020-07-01T00:00:00]", "[Time].[Month].&[2020-08-01T00:00:00]" _
        , "[Time].[Month].&[2020-09-01T00:00:00]", _
        "[Time].[Month].&[2020-10-01T00:00:00]", "[Time].[Month].&[2020-11-01T00:00:00]" _
        , "[Time].[Month].&[2020-12-01T00:00:00]", _
        "[Time].[Month].&[2021-01-01T00:00:00]", "[Time].[Month].&[2021-02-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'Apr20-Mar21'
Sub Step_3_DatePeriodFilter_d_Apr20_Mar21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-04-01T00:00:00]", _
        "[Time].[Month].&[2020-05-01T00:00:00]", "[Time].[Month].&[2020-06-01T00:00:00]" _
        , "[Time].[Month].&[2020-07-01T00:00:00]", _
        "[Time].[Month].&[2020-08-01T00:00:00]", "[Time].[Month].&[2020-09-01T00:00:00]" _
        , "[Time].[Month].&[2020-10-01T00:00:00]", _
        "[Time].[Month].&[2020-11-01T00:00:00]", "[Time].[Month].&[2020-12-01T00:00:00]" _
        , "[Time].[Month].&[2021-01-01T00:00:00]", _
        "[Time].[Month].&[2021-02-01T00:00:00]", "[Time].[Month].&[2021-03-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'May20-Apr21'
Sub Step_3_DatePeriodFilter_e_May20_Apr21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-05-01T00:00:00]", _
        "[Time].[Month].&[2020-06-01T00:00:00]", "[Time].[Month].&[2020-07-01T00:00:00]" _
        , "[Time].[Month].&[2020-08-01T00:00:00]", _
        "[Time].[Month].&[2020-09-01T00:00:00]", "[Time].[Month].&[2020-10-01T00:00:00]" _
        , "[Time].[Month].&[2020-11-01T00:00:00]", _
        "[Time].[Month].&[2020-12-01T00:00:00]", "[Time].[Month].&[2021-01-01T00:00:00]" _
        , "[Time].[Month].&[2021-02-01T00:00:00]", _
        "[Time].[Month].&[2021-03-01T00:00:00]", "[Time].[Month].&[2021-04-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'Jun20-May21'
Sub Step_3_DatePeriodFilter_f_Jun20_May21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-06-01T00:00:00]", _
        "[Time].[Month].&[2020-07-01T00:00:00]", "[Time].[Month].&[2020-08-01T00:00:00]" _
        , "[Time].[Month].&[2020-09-01T00:00:00]", _
        "[Time].[Month].&[2020-10-01T00:00:00]", "[Time].[Month].&[2020-11-01T00:00:00]" _
        , "[Time].[Month].&[2020-12-01T00:00:00]", _
        "[Time].[Month].&[2021-01-01T00:00:00]", "[Time].[Month].&[2021-02-01T00:00:00]" _
        , "[Time].[Month].&[2021-03-01T00:00:00]", _
        "[Time].[Month].&[2021-04-01T00:00:00]", "[Time].[Month].&[2021-05-01T00:00:00]" _
        )
    
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'Jul20-Jun21'
Sub Step_3_DatePeriodFilter_g_Jul20_Jun21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-07-01T00:00:00]", _
        "[Time].[Month].&[2020-08-01T00:00:00]", "[Time].[Month].&[2020-09-01T00:00:00]" _
        , "[Time].[Month].&[2020-10-01T00:00:00]", _
        "[Time].[Month].&[2020-11-01T00:00:00]", "[Time].[Month].&[2020-12-01T00:00:00]" _
        , "[Time].[Month].&[2021-01-01T00:00:00]", _
        "[Time].[Month].&[2021-02-01T00:00:00]", "[Time].[Month].&[2021-03-01T00:00:00]" _
        , "[Time].[Month].&[2021-04-01T00:00:00]", _
        "[Time].[Month].&[2021-05-01T00:00:00]", "[Time].[Month].&[2021-06-01T00:00:00]" _
        )
    
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'Aug20-Jul21'
Sub Step_3_DatePeriodFilter_h_Aug20_Jul21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-08-01T00:00:00]", _
        "[Time].[Month].&[2020-09-01T00:00:00]", "[Time].[Month].&[2020-10-01T00:00:00]" _
        , "[Time].[Month].&[2020-11-01T00:00:00]", _
        "[Time].[Month].&[2020-12-01T00:00:00]", "[Time].[Month].&[2021-01-01T00:00:00]" _
        , "[Time].[Month].&[2021-02-01T00:00:00]", _
        "[Time].[Month].&[2021-03-01T00:00:00]", "[Time].[Month].&[2021-04-01T00:00:00]" _
        , "[Time].[Month].&[2021-05-01T00:00:00]", _
        "[Time].[Month].&[2021-06-01T00:00:00]", "[Time].[Month].&[2021-07-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub


'Sep20-Aug21'
Sub Step_3_DatePeriodFilter_i_Sep20_Aug21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-09-01T00:00:00]", _
        "[Time].[Month].&[2020-10-01T00:00:00]", "[Time].[Month].&[2020-11-01T00:00:00]" _
        , "[Time].[Month].&[2020-12-01T00:00:00]", _
        "[Time].[Month].&[2021-01-01T00:00:00]", "[Time].[Month].&[2021-02-01T00:00:00]" _
        , "[Time].[Month].&[2021-03-01T00:00:00]", _
        "[Time].[Month].&[2021-04-01T00:00:00]", "[Time].[Month].&[2021-05-01T00:00:00]" _
        , "[Time].[Month].&[2021-06-01T00:00:00]", _
        "[Time].[Month].&[2021-07-01T00:00:00]", "[Time].[Month].&[2021-08-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'Oct20-Sep21'
Sub Step_3_DatePeriodFilter_j_Oct20_Sep21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-10-01T00:00:00]", _
        "[Time].[Month].&[2020-11-01T00:00:00]", "[Time].[Month].&[2020-12-01T00:00:00]" _
        , "[Time].[Month].&[2021-01-01T00:00:00]", _
        "[Time].[Month].&[2021-02-01T00:00:00]", "[Time].[Month].&[2021-03-01T00:00:00]" _
        , "[Time].[Month].&[2021-04-01T00:00:00]", _
        "[Time].[Month].&[2021-05-01T00:00:00]", "[Time].[Month].&[2021-06-01T00:00:00]" _
        , "[Time].[Month].&[2021-07-01T00:00:00]", _
        "[Time].[Month].&[2021-08-01T00:00:00]", "[Time].[Month].&[2021-09-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'Nov20-Oct21'
Sub Step_3_DatePeriodFilter_k_Nov20_Oct21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-11-01T00:00:00]", _
        "[Time].[Month].&[2020-12-01T00:00:00]", "[Time].[Month].&[2021-01-01T00:00:00]" _
        , "[Time].[Month].&[2021-02-01T00:00:00]", _
        "[Time].[Month].&[2021-03-01T00:00:00]", "[Time].[Month].&[2021-04-01T00:00:00]" _
        , "[Time].[Month].&[2021-05-01T00:00:00]", _
        "[Time].[Month].&[2021-06-01T00:00:00]", "[Time].[Month].&[2021-07-01T00:00:00]" _
        , "[Time].[Month].&[2021-08-01T00:00:00]", _
        "[Time].[Month].&[2021-09-01T00:00:00]", "[Time].[Month].&[2021-10-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'Dec20-Nov21'
Sub Step_3_DatePeriodFilter_l_Dec20_Nov21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2020-12-01T00:00:00]", _
        "[Time].[Month].&[2021-01-01T00:00:00]", "[Time].[Month].&[2021-02-01T00:00:00]" _
        , "[Time].[Month].&[2021-03-01T00:00:00]", _
        "[Time].[Month].&[2021-04-01T00:00:00]", "[Time].[Month].&[2021-05-01T00:00:00]" _
        , "[Time].[Month].&[2021-06-01T00:00:00]", _
        "[Time].[Month].&[2021-07-01T00:00:00]", "[Time].[Month].&[2021-08-01T00:00:00]" _
        , "[Time].[Month].&[2021-09-01T00:00:00]", _
        "[Time].[Month].&[2021-10-01T00:00:00]", "[Time].[Month].&[2021-11-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'Jan21-Dec21'
Sub Step_3_DatePeriodFilter_m_Jan21_Dec21()
'
' SetMonthFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields("[Time].[Month].[Month]"). _
        VisibleItemsList = Array("[Time].[Month].&[2021-01-01T00:00:00]", _
        "[Time].[Month].&[2021-02-01T00:00:00]", "[Time].[Month].&[2021-03-01T00:00:00]" _
        , "[Time].[Month].&[2021-04-01T00:00:00]", _
        "[Time].[Month].&[2021-05-01T00:00:00]", "[Time].[Month].&[2021-06-01T00:00:00]" _
        , "[Time].[Month].&[2021-07-01T00:00:00]", _
        "[Time].[Month].&[2021-08-01T00:00:00]", "[Time].[Month].&[2021-09-01T00:00:00]" _
        , "[Time].[Month].&[2021-10-01T00:00:00]", _
        "[Time].[Month].&[2021-11-01T00:00:00]", "[Time].[Month].&[2021-12-01T00:00:00]" _
        )
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// SET MONTH FILTER \\\\\\\\\\\\\\\\\\\'
'                          END'
'---------------------------------------------------------'
'---------------------------------------------------------'

'*********************************************************************************************************'
'*********************************************************************************************************'


'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// SET PLATFORM  FILTER \\\\\\\\\\\\\\\\\\\'
'                          START'
'---------------------------------------------------------'
'---------------------------------------------------------'

Sub Step_4_SetPlatformType_6_154()
'
' SetPlatformType Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[Swt Platform]").VisibleItemsList = Array("")
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[SwtType]").VisibleItemsList = Array( _
        "[Turbine].[SWT Platform -Type].[Swt Platform].&[D6].&[SWT-6.0-154]")
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

Sub Step_4_SetPlatformType_7_154()
'
' SetPlatformType Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[Swt Platform]").VisibleItemsList = Array("")
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[SwtType]").VisibleItemsList = Array( _
        "[Turbine].[SWT Platform -Type].[Swt Platform].&[D6].&[SWT-7.0-154]")
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

Sub Step_4_SetPlatformType_8_154()
'
' SetPlatformType Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[Swt Platform]").VisibleItemsList = Array("")
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[SwtType]").VisibleItemsList = Array( _
        "[Turbine].[SWT Platform -Type].[Swt Platform].&[D6].&[SWT-8.0-154]")
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

Sub Step_4_SetPlatformType_8_167()
'
' SetPlatformType Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[Swt Platform]").VisibleItemsList = Array("")
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[SwtType]").VisibleItemsList = Array( _
        "[Turbine].[SWT Platform -Type].[Swt Platform].&[D6].&[SWT-8.0-167]")
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

Sub Step_4_SetPlatformType_10_193()
'
' SetPlatformType Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[Swt Platform]").VisibleItemsList = Array("")
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[SWT Platform -Type].[SwtType]").VisibleItemsList = Array( _
        "[Turbine].[SWT Platform -Type].[Swt Platform].&[D6].&[SWT-10.0-193]")
        
    'update alarm ranges to be used for the next sheet
    Call GetRangeValues(0)
End Sub

'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// SET PLATFORM  FILTER \\\\\\\\\\\\\\\\\\\'
'                          END'
'---------------------------------------------------------'
'---------------------------------------------------------'


'*********************************************************************************************************'
'*********************************************************************************************************'


'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// Copy alarm report result from the pivot table to a new page breaking the connection and add filters  \\\\\\\\\\\\\\\\\\\'
'                                                        START'
'---------------------------------------------------------'
'---------------------------------------------------------'

Sub Step_5_a_CopyAlarmPivotResultToNewSheetAndAddFilters_alarmCountTechUnav()
'
' copytablenew Macro
'
'
    Call GetRangeValues(2)

    'Select Pivot table result and copy to new sheet'
    '*********************'
    '*********************'
    'REMEMBER TO PROVIDE INPUT IN CELL H1 for what range the table are in'
    '*********************'
    '*********************'
    Dim CurrentTableRange As String
    Dim TopRowColumnRange As String
    CurrentTableRange = Cells(1, "H").Value
    TopRowColumnRange = Cells(1, "I").Value
    Range(CurrentTableRange).Select
    Selection.Copy
    Sheets.Add After:=ActiveSheet
    Range("A15").Select
    ActiveSheet.Paste
    
    
    '*********************'
    '*********************'
    'REMEMBER TO CHANGE RANGE IN CASE THE RESULT LOOKS ODD'
    '*********************'
    '*********************'
    'rename cells  who have wrong naming'
    Range("A16").Select
    ActiveCell.FormulaR1C1 = "Alarm code"
    
    'this might need change depending if there not are 12 month avalible of data'
    'note: this could be made more flexible someway - but currently it more or less does the job'
    Range("Z16").Select
    ActiveCell.FormulaR1C1 = "Total Alarm Count"
     
    'this might need change depending if there not are 12 month avalible of data'
    'note: this could be made more flexible someway - but currently it more or less does the job'
    Range("AA16").Select
    ActiveCell.FormulaR1C1 = "Total Technical Unavailability - Alarm (%)"
    
    'add filters to the table'
    Range(TopRowColumnRange).Select
    Selection.AutoFilter
    'Copy grand total row to the top of the sheet'
    'If it looks odd, can this be removed and done manually'
    Range("A16").Select
    Selection.End(xlDown).Select
    Range(Selection, Selection.End(xlToRight)).Select
    Selection.Cut
    Selection.End(xlToLeft).Select
    Selection.End(xlUp).Select
    Range("A15").Select
    ActiveWindow.SmallScroll Down:=-12
    Range("A14").Select
    ActiveSheet.Paste
    Cells.Select
    Cells.EntireColumn.AutoFit
    
    'add owner, group and sub group columns to the table
    Call AddExtraColumns
    'add settings to locate rights columns
    Call TableColumnSettingsForFinalReport
    
End Sub

'---------------------------------------------------------'
'---------------------------------------------------------'
'//////////////////// Copy alarm report result from the pivot table to a new page breaking the connection and add filters  \\\\\\\\\\\\\\\\\\\'
'                                                        START'
'---------------------------------------------------------'
'---------------------------------------------------------'

Sub Step_5_b_CopyAlarmPivotResultToNewSheetAndAddFiltersForAlarmCount()
'
' copytablenew Macro
'
'

    Call GetRangeValues(0)
    'Select Pivot table result and copy to new sheet'
    '*********************'
    '*********************'
    'REMEMBER TO PROVIDE INPUT IN CELL H1 for what range the table are in'
    '*********************'
    '*********************'
    Dim CurrentTableRange As String
    Dim TopRowColumnRange As String
    CurrentTableRange = Cells(1, "H").Value
    TopRowColumnRange = Cells(1, "I").Value
    Range(CurrentTableRange).Select
    Selection.Copy
    Sheets.Add After:=ActiveSheet
    Range("A15").Select
    ActiveSheet.Paste
    
    Rows("16:16").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    '*********************'
    '*********************'
    'REMEMBER TO CHANGE RANGE IN CASE THE RESULT LOOKS ODD'
    '*********************'
    '*********************'
    'rename cells  who have wrong naming'
    Range("A16").Select
    ActiveCell.FormulaR1C1 = "Alarm code"
    
    'this might need change depending if there not are 12 month avalible of data'
    'note: this could be made more flexible someway - but currently it more or less does the job'
    Range("Z16").Select
    ActiveCell.FormulaR1C1 = "Total Alarm Count"
     
    'this might need change depending if there not are 12 month avalible of data'
    'note: this could be made more flexible someway - but currently it more or less does the job'
    Range("AA16").Select
    ActiveCell.FormulaR1C1 = "Total Technical Unavailability - Alarm (%)"
    
    'add filters to the table'
    Range(TopRowColumnRange).Select
    Selection.AutoFilter
    'Copy grand total row to the top of the sheet'
    'If it looks odd, can this be removed and done manually'
    Range("A16").Select
    Selection.End(xlDown).Select
    Range(Selection, Selection.End(xlToRight)).Select
    Selection.Cut
    Selection.End(xlToLeft).Select
    Selection.End(xlUp).Select
    Range("A15").Select
    ActiveWindow.SmallScroll Down:=-12
    Range("A14").Select
    ActiveSheet.Paste
    Cells.Select
    Cells.EntireColumn.AutoFit
    
    'add owner, group and sub group columns to the table
    Call AddExtraColumns
    'add settings to locate rights columns
    Call TableColumnSettingsForFinalReport
    
    'change the start,end row numb and which row the descriptiom, group and subgroup are writtenin
    Call ChangeRangeAndColumnDescriptionLocation
    
End Sub

    'add owner, group and sub group columns to the table
Private Sub AddExtraColumns()
    Columns("B:F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("B16").Select
    ActiveCell.FormulaR1C1 = "Description"
    Range("C16").Select
    ActiveCell.FormulaR1C1 = "Group"
    Range("D16").Select
    ActiveCell.FormulaR1C1 = "Sub Group"
    Range("E16").Select
    ActiveCell.FormulaR1C1 = "Owner"
    Range("F16").Select
    ActiveCell.FormulaR1C1 = "AlarmType"
End Sub

Private Sub TableColumnSettingsForFinalReport()
    Cells(1, "A").Value = "All alarms sheet name"
    Cells(1, "B").Value = "AllAlarms"
    Cells(1, "C").Value = "Alarm owner/group overview sheet"
    
    Cells(1, "D").Value = "Alarm Type column"
    Cells(1, "E").Value = "F"
    Cells(1, "F").Value = "Alarm owner/group overview sheet"
    
    Cells(1, "F").Value = "Location of sheet:"
    Cells(1, "G").Value = "https://siemensgamesa.sharepoint.com/teams/OG00570/Product/TurbineAlarms/Alarms_Groups_And_Owners.xlsx"
    
    Cells(2, "A").Value = "Description column"
    Cells(2, "B").Value = "B"
    Cells(2, "C").Value = "Alarm owner/group overview sheet"
    
    Cells(3, "A").Value = "Group Column"
    Cells(3, "B").Value = "C"
    Cells(3, "C").Value = "Alarm owner/group overview sheet"
    
    Cells(4, "A").Value = "Sub Group Column"
    Cells(4, "B").Value = "D"
    Cells(4, "C").Value = "Alarm owner/group overview sheet"
    
    Cells(5, "A").Value = "Owner column"
    Cells(5, "B").Value = "E"
    Cells(5, "C").Value = "Alarm owner/group overview sheet"
    
    Cells(7, "A").Value = "Current sheet name"
    Cells(7, "B").Value = ActiveSheet.Name
    Cells(7, "C").Value = "Alarm 12 month table"
    
    Cells(7, "D").Value = "Alarm Type Column"
    Cells(7, "E").Value = "F"
    Cells(7, "F").Value = "Alarm 12 month table"
    
    'find start and end row no for the alarm list
        Dim row_start_no As String
        Dim row_end_no As String
        row_start_no = "17"
        
        Range("A16").Select
        Selection.End(xlDown).Select
        row_end_no = ActiveCell.Row
        Range("A16").Select
    
    Cells(8, "A").Value = "Alarm start row number"
    Cells(8, "B").Value = row_start_no
    Cells(8, "C").Value = "Alarm 12 month table"
    
    Cells(9, "A").Value = "Alarm end row number"
    Cells(9, "B").Value = row_end_no
    Cells(9, "C").Value = "Alarm 12 month table"
    
    Call LeftAlignCell("B8")
    Call LeftAlignCell("B9")
    
    Cells(10, "A").Value = "Description Column"
    Cells(10, "B").Value = "B"
    Cells(10, "C").Value = "Alarm 12 month table"
    
    Cells(11, "A").Value = "Group Column"
    Cells(11, "B").Value = "C"
    Cells(11, "C").Value = "Alarm 12 month table"
    
    Cells(12, "A").Value = "Sub Group column"
    Cells(12, "B").Value = "D"
    Cells(12, "C").Value = "Alarm 12 month table"
    
    Cells(13, "A").Value = "Owner Column"
    Cells(13, "B").Value = "E"
    Cells(13, "C").Value = "Alarm 12 month table"
    
    Columns("A:D").Select
    Columns("A:D").EntireColumn.AutoFit
    

End Sub

'find alarm owner, group and sub group
Sub Find_Alarm_Data()
   'Loop thorugh
    
    'In AlLAlarmsSheet A_
    Dim A_All_alarms_sheet_name As String
    Dim A_Description_column As String
    Dim A_Group_column As String
    Dim A_SubGroup_column As String
    Dim A_Owner_column As String
    Dim A_AlarmType_column As String
    

    'In Alarm 12 month sheet B_
    Dim b_sheet_name As String
    Dim B_Alarm_start_row_numb As String
    Dim B_Alarm_end_row_numb As String
    Dim B_Description_column As String
    Dim b_group_column As String
    Dim B_SubGroup_column As String
    Dim b_owner_column As String
    Dim B_AlarmType_column As String
    
    'other variables
    Dim Alarms_not_found_in_all_alarms_list As String

    'fetch data from the settings
    'from All Alarms Sheet
    A_All_alarms_sheet_name = Cells(1, "B")
    A_Description_column = Cells(2, "B")
    A_Group_column = Cells(3, "B")
    A_SubGroup_column = Cells(4, "B")
    A_Owner_column = Cells(5, "B")
    A_AlarmType_column = Cells(1, "E")
    
    'From Alarm 12 month sheet
    b_sheet_name = Cells(7, "B")
    B_Alarm_start_row_numb = Cells(8, "B")
    B_Alarm_end_row_numb = Cells(9, "B")
    B_Description_column = Cells(10, "B")
    b_group_column = Cells(11, "B")
    B_SubGroup_column = Cells(12, "B")
    b_owner_column = Cells(13, "B")
    B_AlarmType_column = Cells(7, "E")
    
    'temp alarm info place holders
    Dim temp_Current_Alarm_Cell_no
    Dim temp_Current_Alarm_No
    Dim temp_Owner As String
    Dim temp_Group As String
    Dim temp_SubGroup As String
    Dim temp_Description As String
    Dim temp_AlarmType As String
    
    'loop start, loop through all alarms and find the description, owner, group and sub group
    For i = B_Alarm_start_row_numb To B_Alarm_end_row_numb

    'select current alarm cell
    temp_Current_Alarm_Cell_no = "A" & i
    Range(temp_Current_Alarm_Cell_no).Select
    'get current alarm number
    temp_Current_Alarm_No = ActiveCell.Value
    
    'go to All Alarms Sheet
    Sheets(A_All_alarms_sheet_name).Select
    
    'Search for the alarm
    'remember to update range for this once new alarms are introduced
    Dim rgFound As Range
    '"A8:A1624"
    Set rgFound = Range("A8:A" & LastRowInOneColumn("A")).Find(temp_Current_Alarm_No)
    
    If rgFound Is Nothing Then
        Alarms_not_found_in_all_alarms_list = Alarms_not_found_in_all_alarms_list & "," & temp_Current_Alarm_No
        'Sheets(B_Sheet_name).Select
        'Cells(i, B_Description_column).Value = "Alarm has been removed from the software"
        'Sheets(A_All_alarms_sheet_name).Select
    'If it is found
        temp_Group = "Alarm deleted"
        temp_SubGroup = "Alarm deleted"
        temp_Owner = "Alarm deleted"
        temp_Description = "Alarm deleted, no longer part of latest software"
        temp_AlarmType = "Alarm deleted"
    Else
    Range(rgFound.Address).Select
    'Copy Owner, Group, Sub group
    temp_Group = Cells(ActiveCell.Row, A_Group_column).Value
    temp_SubGroup = Cells(ActiveCell.Row, A_SubGroup_column).Value
    temp_Owner = Cells(ActiveCell.Row, A_Owner_column).Value
    temp_Description = Cells(ActiveCell.Row, A_Description_column).Value
    temp_AlarmType = Cells(ActiveCell.Row, A_AlarmType_column).Value
    
    End If

    
    
    'Else not add to list
    'Go back to alarm 12 month sheet
    Sheets(b_sheet_name).Select

    
    'paste owner, group, sub group
    Cells(i, B_Description_column).Value = temp_Description
    Cells(i, b_owner_column).Value = temp_Owner
    Cells(i, b_group_column).Value = temp_Group
    Cells(i, B_SubGroup_column).Value = temp_SubGroup
    Cells(i, B_AlarmType_column).Value = temp_AlarmType

    'go to next element
    
    'Reset all variables so they not potentially can be copied from current iteration to the next
    'in case the alarm not has been found in the list
    temp_Current_Alarm_Cell_no = ""
    temp_Current_Alarm_No = ""
    temp_Owner = ""
    temp_Group = ""
    temp_SubGroup = ""
    temp_Description = ""
    temp_AlarmType = ""
    
    Next i
    
    'add the list of alarms not found
    Cells(2, "D").Value = "Alarms not found:"
    Cells(2, "E").Value = Alarms_not_found_in_all_alarms_list
    Columns("C:E").Select
    Selection.ColumnWidth = 34.3
    
    
End Sub


Public Function LastRowInOneColumn(column_letter As String) As String
'Find the last used row in a Column: column A in this example
    Dim LastRow As Long
    With ActiveSheet
        LastRow = .Cells(.Rows.Count, column_letter).End(xlUp).Row
    End With
    LastRowInOneColumn = LastRow
End Function

'Left aligning cell number like "b8"
Private Sub LeftAlignCell(CellRange As String)
    Range(CellRange).Select
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
End Sub


'Convert a column number to a letter
Public Function columnLetter(ColumnNumber As Long) As String
    columnLetter = Split(Cells(1, ColumnNumber).Address(True, False), "$")(0)
End Function


'Get ranges for start and end of the current alarm sheet - and add ranges to be used on the next sheet beening created
Private Sub GetRangeValues(AddExtraColumns As Integer)

        'find start and end row no for the alarm list
        Dim row_start_no As String
        Dim row_end_no As String
        Dim first_alarm_row_no As Integer
        Dim tempRowNo As Integer
        
        Cells.Find(What:="Row Labels", After:=ActiveCell, LookIn:=xlFormulas, _
        LookAt:=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, _
        MatchCase:=False, SearchFormat:=False).Activate
        row_start_no = ActiveCell.Row
        tempRowNo = ActiveCell.Row
        
        first_alarm_row_no = ActiveCell.Row + 1
        '
        'row_start_no = "4"
        
        Range("A5").Select
        Selection.End(xlDown).Select
        row_end_no = ActiveCell.Row
        Range("A4").Select
        
        
    
    
        'get length of columns in the table
        'Range("A5").Select----- MAYBE REMOVE THIS
        'Dim tempRange As String
       ' tempRange = "A:" & row_start_no
        Range("A" & row_start_no).Select
        Selection.End(xlToRight).Select
    
        Dim columnEndNumb As Long
        columnEndNumb = ActiveCell.Column + AddExtraColumns
        Dim Last_column_letter As String
        Last_column_letter = columnLetter(columnEndNumb)
    Cells(5, columnEndNumb).Select
    Cells(1, "I").Value = "A16:" & Last_column_letter & "16"
    If AddExtraColumns = 2 Then
        row_start_no = tempRowNo - 1
    End If
    Cells(1, "H").Value = "A" & row_start_no & ":" & Last_column_letter & row_end_no
    Range("I1").Select
End Sub




'this can perhaps be deleted
Function AddSparkLineToCurrentSheet()
    Cells.Find(What:="Grand Total", After:=ActiveCell, LookIn:=xlFormulas, _
    LookAt:=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, _
    MatchCase:=False, SearchFormat:=False).Activate
    
    Cells.FindNext(After:=ActiveCell).Activate
    Dim columnEndNumb As Long
    Dim columnEndLetter As String
    columnEndNumb = ActiveCell.Column + 1
    columnEndLetter = columnLetter(columnEndNumb)
    
    
    Dim startRow As String
    Dim endRow As String
    startRow = Cells(8, "B")
    endRow = Cells(9, "B")
    
    Dim myDataRng As String
    
    For i = startRow To endRow
    
    myDataRng = "F" & i & ":Q" & i
    'Dim myLetter As String
    'myLetter = CStr(i)
    Call AddSparklineService(myDataRng, CStr(i), columnEndLetter)
    Next i
    
    Dim trendlineDescriptionRow As Integer
    trendlineDescriptionRow = Cells(8, "B") - 1
    Cells(trendlineDescriptionRow, columnEndLetter).Value = "12 Month Trendline"
End Function


 
 'function to add sparklines after formatting have been added to the alarm count + alarm tech unav sheet
 Sub ReAddSparkLineAuto(sheetname As String)
    Dim startRow As String
    Dim endRow As String
    
    Sheets(sheetname).Select
    
    Columns("S:S").Select
    Selection.Delete Shift:=xlToLeft
    Range("S15").Select
    ActiveCell.FormulaR1C1 = "12 Month Trendline"
    
    'update numbers to first alarm row and lasst alarm row
    startRow = Cells(8, "B")
    endRow = Cells(9, "B")
    '327
    
    Dim myDataRng As String
    
    For i = startRow To endRow
    
    'update letters to first month column and the last month column
    myDataRng = "G" & i & ":R" & i
    'Dim myLetter As String
    'myLetter = CStr(i)
    'update the "S" to be the destination colimn for the sparkline
    Call AddSparklineService(myDataRng, CStr(i), "S")
    Next i
 End Sub

'service function to add sparklines to the sheets
Private Sub AddSparklineService(DataRng As String, DestRowNumb As String, DestColumnLetter As String)
    Range(DataRng).Select
    Application.CutCopyMode = False
    Dim rng As String
    rng = DestColumnLetter & DestRowNumb
    Range(rng).Select
    Dim rngWithDollarSign As String
    rngWithDollarSign = "$" & DestColumnLetter & "$" & DestRowNumb
    Range(rngWithDollarSign).SparklineGroups.Add Type:=xlSparkLine, SourceData:=DataRng
    Selection.SparklineGroups.item(1).SeriesColor.Color = 9592887
    Selection.SparklineGroups.item(1).SeriesColor.TintAndShade = 0
    Selection.SparklineGroups.item(1).Points.Negative.Color.Color = 208
    Selection.SparklineGroups.item(1).Points.Negative.Color.TintAndShade = 0
    Selection.SparklineGroups.item(1).Points.Markers.Color.Color = 208
    Selection.SparklineGroups.item(1).Points.Markers.Color.TintAndShade = 0
    Selection.SparklineGroups.item(1).Points.Highpoint.Color.Color = 208
    Selection.SparklineGroups.item(1).Points.Highpoint.Color.TintAndShade = 0
    Selection.SparklineGroups.item(1).Points.Lowpoint.Color.Color = 208
    Selection.SparklineGroups.item(1).Points.Lowpoint.Color.TintAndShade = 0
    Selection.SparklineGroups.item(1).Points.Firstpoint.Color.Color = 208
    Selection.SparklineGroups.item(1).Points.Firstpoint.Color.TintAndShade = 0
    Selection.SparklineGroups.item(1).Points.Lastpoint.Color.Color = 208
    Selection.SparklineGroups.item(1).Points.Lastpoint.Color.TintAndShade = 0
    Application.CutCopyMode = False
    ActiveCell.SparklineGroups.item(1).DisplayBlanksAs = xlZero
    ActiveCell.SparklineGroups.item(1).DisplayHidden = False
    Columns("S:S").ColumnWidth = 21.57
End Sub


Sub Filter_AddTechUnavFilter()
'
' AddTechUnavFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").AddDataField ActiveSheet.PivotTables( _
        "PivotTable1").CubeFields("[Measures].[Technical Unavailability - Alarm (%)]")
        
    Call GetRangeValues(0)
End Sub
Sub Filter_RemoveTechUnavaFilter()
'
' RemoveTechUnavaFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Measures].[Technical Unavailability - Alarm (%)]").Orientation = xlHidden
    
    Call GetRangeValues(0)
End Sub

Sub Filter_Add_ReportedTurbines()
'
' Filter_Add_ReportedTurbines Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").AddDataField ActiveSheet.PivotTables( _
        "PivotTable1").CubeFields("[Measures].[Reported Turbines]")
End Sub
Sub Filter_Remove_ReportedTurbines()
'
' Filter_Remove_ReportedTurbines Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Measures].[Reported Turbines]").Orientation = xlHidden
End Sub


Sub Filter_RemoveAlarmCountFilter()
'
' RemoveAlarmCount Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").CubeFields("[Measures].[Alarm Count]"). _
        Orientation = xlHidden
        
    Call GetRangeValues(0)
End Sub
Sub Filter_AddAlarmCountFilter()
'
' AddAlarmCountFilter Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").AddDataField ActiveSheet.PivotTables( _
        "PivotTable1").CubeFields("[Measures].[Alarm Count]")
        
    Call GetRangeValues(0)
End Sub

Sub a_6_create_lc_top10_alarm_dist_on_park_and_turbine_level_sheets()
    Call b_1_Filter_AddTechUnav_remove_reported_turbines_add_country_park_name
    Call b_2_Filter_top10_LC_alarms_in_the_raw_dataset
    Call b_Expland_Countries
    
    'copy data to a new sheet
    'select data
    Range("A4:M" & HelperModule.LastRowInOneColumn("A")).Select
    Selection.Copy
    Sheets.Add After:=ActiveSheet
    Range("A2").Select
    ActiveSheet.Paste
    ActiveSheet.Name = "Sheet9"
    Columns("B:B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    'autofit all columns
    Cells.Select
    Cells.EntireColumn.AutoFit
    Range("B2") = "Description"
    Call HelperModule.Change_column_width("Sheet9", "35", "b")
    
    Sheets("Sheet1").Select
    
    Call b_Expand_Sites
    
    'copy data to a new sheet
    Range("A4:M" & HelperModule.LastRowInOneColumn("A")).Select
    Selection.Copy
    Sheets.Add After:=ActiveSheet
    Range("A2").Select
    ActiveSheet.Paste
    ActiveSheet.Name = "Sheet10"
    Columns("B:B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("B2") = "Description"
    'autofit all columns
    Cells.Select
    Cells.EntireColumn.AutoFit
    Call HelperModule.Change_column_width("Sheet10", "35", "b")
    
    'Rename sheets so they have their correct names
    Call RenameSheets
    
    'Loop thorugh site and turbine distribution sheets and find descriptions for the alarms
    Dim LC_view_sheet_name As String
    LC_view_sheet_name = "L&C Alarm Overview"
    
    Sheets("Top10LC alarm_turb_distribution").Select
    For i = 26 To 35
        Cells.Find(What:=Worksheets(LC_view_sheet_name).Range("A" & i), After:=ActiveCell, LookIn:=xlFormulas, LookAt _
        :=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, MatchCase:= _
        False, SearchFormat:=False).Activate
        'Debug.Print Worksheets(LC_view_sheet_name).Range("A" & i)
        Call b_Find_Alarm_description_for_alarm_disbrution_sheets
        On Error Resume Next
    Next i
    
    Sheets("Top10LC alarm_site_distribution").Select
    For i = 26 To 35
        Cells.Find(What:=Worksheets(LC_view_sheet_name).Range("A" & i), After:=ActiveCell, LookIn:=xlFormulas, LookAt _
        :=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, MatchCase:= _
        False, SearchFormat:=False).Activate
        'Debug.Print Worksheets(LC_view_sheet_name).Range("A" & i)
        Call b_Find_Alarm_description_for_alarm_disbrution_sheets
        On Error Resume Next
    Next i

    'Hide sheets which not are relevant for people to see in the report
    Sheets("All Groups").Select
    ActiveWindow.SelectedSheets.Visible = False
    Sheets("AlarmOwnerGroups").Select
    ActiveWindow.SelectedSheets.Visible = False
    Sheets("AllAlarms").Select
    ActiveWindow.SelectedSheets.Visible = False
End Sub


'used for top 10 lc alarm by tech unav in raw data sheet ("Sheet1")
'function to add figure out which park a given alarm has tech uanv on - for raw data.
Function b_1_Filter_AddTechUnav_remove_reported_turbines_add_country_park_name()
'
' Macro1 Macro
'

'
    Dim CurrentFileName As String
    CurrentFileName = ThisWorkbook.Name & "!Filter_AddTechUnavFilter"
    Sheets("sheet1").Select
    'Application.Run "LC_alarm_Report_generator.xlsm!Filter_AddTechUnavFilter"
    Application.Run CurrentFileName
    'Range("L14").Select
    ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Measures].[Reported Turbines]").Orientation = xlHidden
    With ActiveSheet.PivotTables("PivotTable1").CubeFields( _
        "[Turbine].[Country - Park - Name]")
        .Orientation = xlRowField
        .Position = 2
    End With
End Function

'used for top 10 lc alarm by tech unav in raw data sheet ("Sheet1")
'Takes top 10 LC alarm tech unav wise and filter the raw data ("sheet1" ), showing which country/parks/sites  are affected
Function b_2_Filter_top10_LC_alarms_in_the_raw_dataset()
'
' Macro1 Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Alarm Generic Information].[Alarm Code].[Alarm Code]").VisibleItemsList = _
        Array("[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A26") & "]", _
        "[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A26") & "]", _
        "[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A27") & "]", _
        "[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A28") & "]", _
        "[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A29") & "]", _
        "[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A30") & "]", _
        "[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A31") & "]", _
        "[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A32") & "]", _
        "[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A33") & "]", _
        "[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A34") & "]", _
        "[Alarm Generic Information].[Alarm Code].&[" & Worksheets("sheet8").Range("A35") & "]")
    ActiveWindow.SmallScroll Down:=-6
End Function


'used for top 10 lc alarm by tech unav in raw data sheet ("Sheet1")
'alt a + j
Function b_Expland_Countries()
'
' Macro3 Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotSelect _
        "'[Turbine].[Country - Park - Name].[Country]'[All]", xlLabelOnly + xlFirstRow _
        , True
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[Country - Park - Name].[Country]").DrilledDown = True
    ActiveSheet.PivotTables("PivotTable1").PivotSelect _
        "'[Turbine].[Country - Park - Name].[Park]'[All]", xlLabelOnly + xlFirstRow, _
        True

End Function

'used for top 10 lc alarm by tech unav in raw data sheet ("Sheet1")
'alt a + j
Function b_Expand_Sites()
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[Country - Park - Name].[Park]").DrilledDown = True
    ActiveSheet.PivotTables("PivotTable1").PivotSelect _
        "'[Turbine].[Country - Park - Name].[Park]'[All]", xlLabelOnly + xlFirstRow, _
        True
End Function

'used for top 10 lc alarm by tech unav in raw data sheet ("Sheet1")
'alt a + h
Function b_Collapse_Countries()
'
' Macro3 Macro
'

'
    ActiveSheet.PivotTables("PivotTable1").PivotSelect _
        "'[Turbine].[Country - Park - Name].[Country]'[All]", xlLabelOnly + xlFirstRow _
        , True
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[Country - Park - Name].[Country]").DrilledDown = True
    ActiveSheet.PivotTables("PivotTable1").PivotSelect _
        "'[Turbine].[Country - Park - Name].[Park]'[All]", xlLabelOnly + xlFirstRow, _
        True

End Function
'used for top 10 lc alarm by tech unav in raw data sheet ("Sheet1")
'alt a + h
Function b_Collapse_Sites()
    ActiveSheet.PivotTables("PivotTable1").PivotFields( _
        "[Turbine].[Country - Park - Name].[Park]").DrilledDown = True
    ActiveSheet.PivotTables("PivotTable1").PivotSelect _
        "'[Turbine].[Country - Park - Name].[Park]'[All]", xlLabelOnly + xlFirstRow, _
        True
End Function

'remember to add a empty column next to the alarm numb for adding the alarm description text
'select the cell the alarm we wish to find the description for.
'the method will now go to the LC overview sheet and search for the alarm and find the corrosponding alarm
'description text and add it in the column next to the alarm numb in the distribution sheet
'the sheets needs to called it's final names "Sheet8", "sheet7" etc will not work
'works for both:
'Top10LC alarm_turb_distribution
'Top10LC alarm_site_distribution
Function b_Find_Alarm_description_for_alarm_disbrution_sheets()
'
'Could also be solved to loop throgh all top 10 alarms on LC overview
'copy alarm and description in a array
'one arrary for numb
'one arrary for description
'go to the first ditribution sheet.
'add a new column
'search for the first alarm
'add the description in the next column
'until the end
'then go to the last distribution sheet and do the same
'

'
    Dim currentCellRange As String
    Dim destinationCellRange As String
    Dim currentSheetName As String
    Dim LC_overview_Sheetname As String
    Dim alarm_no As String
    
    'get the current cell numb
    currentCellRange = ActiveCell.Address
    'set the destination cell range for the alarm description
    destinationCellRange = "B" & ActiveCell.Row
    'find current sheetname
    currentSheetName = ActiveSheet.Name
    'get current alarm numb we are going to use for our search
    alarm_no = Range(currentCellRange)
    'set sheet name for LC alarm overview
    LC_overview_Sheetname = "L&C Alarm Overview"
    
    
    'Go to the search sheet containing the information we need
    Sheets(LC_overview_Sheetname).Select
    'select the top cell on the sheet to ensure we just get the first result occuring on the sheet
    'we need to do this so we get a fresh start everytime we start run the method
    Range("a1").Select
    'search for the alarm and select the result cell
    Cells.Find(What:=alarm_no, After:=ActiveCell, LookIn:=xlFormulas, LookAt _
        :=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, MatchCase:= _
        False, SearchFormat:=False).Activate
    
    Dim alarm_result_cell_range As String
    Dim alarm_result_row_numb As String
    Dim alarm_result_description_cell_range As String
    
    'get the actual alarm cell numb
    alarm_result_cell_range = ActiveCell.Address
    'find the row numb, used for when we are copying the description text
    alarm_result_row_numb = ActiveCell.Row
    'set result page
    alarm_result_description_cell_range = "B" & alarm_result_row_numb
    

    'go back to the starting sheet
    Sheets(currentSheetName).Select
    'add the alarm description text for the alarm
    Range(destinationCellRange) = Worksheets(LC_overview_Sheetname).Range(alarm_result_description_cell_range)
    'select the description just added cell
    Range(destinationCellRange).Select
    'unbold the cell and wrap text
    Selection.Font.Bold = False
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = True
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Range(currentCellRange).Select
End Function







'Remove alarm count and tech unav filter and add reported turbines filter and adding reported turbines to the tech unav and alarm count sheet
'might break if we dont have 12 months of data.... then remove this function
'missing in the function: need to copy values to both alarm count and tech unav sheet and alarm overview last month sheet with total numbers of turbines
Private Sub GetReportedTurbines(tech_unav_sheet_name As String, alarm_count_sheet_name As String)
    'GO to pivot table sheet
    Sheets("Sheet1").Select
    'set the right filters
    Call Filter_Add_ReportedTurbines
    Call Filter_RemoveAlarmCountFilter
    Call Filter_RemoveTechUnavaFilter
    'Select the range for 12 months data and copy
    Range("B5:M5").Copy
    
    'go to the tech unav sheet and paste the result
    Sheets(tech_unav_sheet_name).Select
    Range("G" & Cells(8, "B") - 2).Select
    ActiveSheet.Paste
    Cells(14, "F") = "Reported Turbines"
    'go to the alarm count sheet and paste the result
    Sheets(alarm_count_sheet_name).Select
    Range("G" & Cells(8, "B") - 2).Select
    ActiveSheet.Paste
    Cells(14, "F") = "Reported Turbines"
End Sub

'Functions who renames the month format from full names (March 2020) to shorter name (Mar 20)
'This function might break if we dont have 12 month full of data - then remove the function if needed
 Private Sub ChangeMonthFormatInDataSheets(sheet_name As String)
    'go to the right sheet
    Sheets(sheet_name).Select
    'Columns for the months
    'Colum 7 (G) to Column 18 (R)
    Dim tempString1 As String
    Dim tempString2 As String
    'loop through all month columns
    For i = 7 To 18
    'get the month and pick the parts needed
        tempString1 = Cells(15, columnLetter(CInt(i)))
        tempString2 = Cells(15, columnLetter(CInt(i)))
        tempString1 = Left(tempString1, 3)
        tempString2 = Right(tempString2, 2)
        'update the month in the cell
        Cells(15, columnLetter(CInt(i))) = "'" & tempString1 & " " & tempString2
    Next i
    
End Sub

'Small modifications to the sheets containng either only Alarm Count or Technical Unavalibility
'method only called in Step_5_b
'this method should be removed - but in order to do that:
'assign the right range ( alarm start end numb ) which is hardcoded in TableColumnSettingsForFinalReport
'write description, group, sub group in the right columns
Function ChangeRangeAndColumnDescriptionLocation()
'
' FinalWrap Macro
'
'
    Range("A16:F16").Select
    Selection.Cut
    Range("A15").Select
    ActiveSheet.Paste
    Rows("16:16").Select
    Selection.Delete Shift:=xlUp
    Range("A15").Select
    Range(Selection, Selection.End(xlToRight)).Select
    Selection.AutoFilter
    Cells.Select
    Selection.ColumnWidth = 17.57
    Cells(8, "B").Value = Cells(8, "B").Value - 1
    Cells(9, "B").Value = Cells(9, "B").Value - 1
End Function





Sub DeleteEntireContentInRow(rowNumb As Integer)
'
' DeleteEntireContentInRow Macro
'

'
    Dim rng As String
    rng = rowNumb & ":" & rowNumb
    Rows(rng).Select
    Selection.ClearContents
End Sub



    
Sub DeleteColumnBySearchWord(searchWord As String)
'
' DeleteColumnBySearchWord Macro
'
'
    Cells.Find(What:=searchWord, After:=ActiveCell, LookIn:=xlFormulas, _
        LookAt:=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, _
        MatchCase:=False, SearchFormat:=False).Activate
    
    Dim columnNumb As Long
    columnNumb = ActiveCell.Column
    Dim myColumnLetter As String
    myColumnLetter = columnLetter(columnNumb)
    Dim rng As String
    rng = myColumnLetter & ":" & myColumnLetter
    Columns(rng).Select
    Selection.Delete Shift:=xlToLeft
End Sub



Private Sub c_step_0_AlarmOverviewBasicSettings()
'add standard settigs to the document
    Dim AlarmCountSheetDescription As String
    AlarmCountSheetDescription = "In alarm count sheet"
    Sheets.Add After:=ActiveSheet
    
    Dim current_sheet_name As String
    current_sheet_name = ActiveSheet.Name
    
    Cells(1, "A").Value = "Alarm overview sheet name"
    Cells(1, "B").Value = current_sheet_name
    
        
    Cells(3, "A").Value = "Alarm count sheet name"
    Cells(3, "B").Value = "Sheet5"
    
    Dim alarm_owner_sheet_name As String
    Dim tempRowNumb As String
    alarm_owner_sheet_name = "AlarmOwnerGroups"
    
    Cells(5, "A").Value = "Alarm owner sheet name"
    Cells(5, "B").Value = alarm_owner_sheet_name
    
    Sheets(alarm_owner_sheet_name).Select
    tempRowNumb = LastRowInOneColumn("A")
    Sheets(current_sheet_name).Select
    
    Dim alarm_group_sheet_name As String
    alarm_group_sheet_name = "All Groups"
    
    Cells(6, "A").Value = "Alarm owner sheet START row"
    Cells(6, "B").Value = "1"
    Cells(7, "A").Value = "Alarm owner sheet END row"
    Cells(7, "B").Value = tempRowNumb
    
    'reset temp value
    tempRowNumb = "empty"
    
    Cells(9, "A").Value = "Alarm group sheet name"
    Cells(9, "B").Value = alarm_group_sheet_name
    
    Sheets(alarm_group_sheet_name).Select
    tempRowNumb = LastRowInOneColumn("A")
    Sheets(current_sheet_name).Select
    
    Cells(10, "A").Value = "Alarm group sheet START row"
    Cells(10, "B").Value = "1"
    Cells(11, "A").Value = "Alarm group sheet END row"
    Cells(11, "B").Value = tempRowNumb
    
    'reset temp value
    tempRowNumb = "empty"
    
    Cells(13, "A").Value = "Last month column in alarm count sheet"
    Cells(13, "B").Value = "R"
    Cells(13, "C").Value = AlarmCountSheetDescription
    Cells(14, "A").Value = "Group column"
    Cells(14, "B").Value = "C"
    Cells(14, "C").Value = AlarmCountSheetDescription
    Cells(15, "A").Value = "Sub group column"
    Cells(15, "B").Value = "D"
    Cells(15, "C").Value = AlarmCountSheetDescription
    Cells(16, "A").Value = "Owner Column"
    Cells(16, "B").Value = "E"
    Cells(16, "C").Value = AlarmCountSheetDescription
    
    Cells(1, "D").Value = "Start row for Alarm owner overview"
    Cells(1, "E").Value = "18"
    
    'format settings
    Cells.Select
    Cells.EntireColumn.AutoFit
    Range("B6:B7").Select
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Range("B10:B11").Select
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Range("A1").Select
End Sub

Function c_step_1_CreateAlarmOverview()
    Dim startRowForTable As Integer
    Dim alarmOverviewSheetName As String
    startRowForTable = Cells(1, "E")
    alarmOverviewSheetName = Cells(1, "B")
    'Debug.Print startRowForTable
    
    Cells(startRowForTable, "A").Value = "Alarm Owner Group"
    Cells(startRowForTable, "B").Value = "Sum of Triggered Alarms"
    Cells(startRowForTable, "C").Value = "Sum of Technical Unavailability"
    
    'Alarm owner sheet settings
    Dim a_sheet_name As String
    Dim a_start_row As Integer
    Dim a_end_row As Integer
    
    a_sheet_name = Cells(5, "B")
    a_start_row = Cells(6, "B")
    a_end_row = Cells(7, "B")
    
    
    'Range.Copy to other worksheets
    'Copy all owner groups from the ownergroup sheet to the overview sheet
    Worksheets(a_sheet_name).Range("A" & a_start_row & ":A" & a_end_row).Copy Worksheets(alarmOverviewSheetName).Range("A" & startRowForTable + 1)
    
    'Create overview with alarm groups
    Dim rowForAlarmGroupOverview As Integer
    rowForAlarmGroupOverview = startRowForTable + a_end_row + 4
    
    Cells(rowForAlarmGroupOverview, "A").Value = "Group"
    Cells(rowForAlarmGroupOverview, "B").Value = "Sub Group"
    Cells(rowForAlarmGroupOverview, "C").Value = "No of alarms"
    Cells(rowForAlarmGroupOverview, "D").Value = "Sum of Technical Unavailability"
    
    Dim b_sheet_name As String
    Dim b_group_start_row As String
    Dim b_sub_group_start_row As String
    
    b_sheet_name = Cells(9, "B")
    b_group_start_row = Cells(10, "B")
    b_sub_group_end_row = Cells(11, "B")
    
    'Range.Copy to other worksheets
    'Copy all alarm groups from the alarm groups sheet to the overview sheet
    Worksheets(b_sheet_name).Range("A" & b_group_start_row & ":B" & b_sub_group_end_row).Copy Worksheets(alarmOverviewSheetName).Range("A" & rowForAlarmGroupOverview + 1)
    Debug.Print rowForAlarmGroupOverview
    
End Function

'Search the last month of the assigned alarm count sheet and calculated how many alarm belong to each owner group
Sub c_step_x2x_GetSumOfAlarmsDataByOwner(a_start_row As Integer, a_end_row As Integer, a_sheetname As String, destinationColumn As String)

    'in alarm count sheet
    Dim b_alarm_Start_Row As Integer
    Dim b_alarm_end_row As Integer
    Dim b_owner_column As String
    Dim b_last_month_column As String
    Dim b_lastMonthRange As String
    Dim b_ownerRange As String
    
    'get start and end row in the alarm count sheet
    b_alarm_Start_Row = Worksheets(a_sheetname).Range("B8")
    b_alarm_end_row = Worksheets(a_sheetname).Range("B9")
    'read settings from overview sheet
    b_last_month_column = Cells(13, "B")
    b_owner_column = Cells(16, "B")
    
    'createa ranges used for the formula based on the settings loaded above
    b_lastMonthRange = b_last_month_column & b_alarm_Start_Row & ":" & b_last_month_column & b_alarm_end_row
    b_ownerRange = b_owner_column & b_alarm_Start_Row & ":" & b_owner_column & b_alarm_end_row
    
    'in overview sheet

    
    
    'loop through the alarm owner list and calculate number of triggered alarmsd find how many alarms each owner group have been triggered for certain row (month)
    For i = a_start_row To a_end_row
        Cells(i, destinationColumn) = Application.WorksheetFunction.SumIf(Sheets(a_sheetname).Range(b_ownerRange), Range("A" & i), Sheets(a_sheetname).Range(b_lastMonthRange))
    Next i
   '-----------------------
   '--------alternative R1C1 style formula for the above, the start_row end_row variables not needed
    'Range("B19").Select
    'ActiveCell.FormulaR1C1 = _
        "=SUMIF(Sheet14!R16C5:R333C5,RC[-1],Sheet14!R16C18:R333C18)"
    'Range("B19").Select
    'Selection.AutoFill Destination:=Range("B19:B36"), Type:=xlFillDefault
    'Range("B19:B36").Select
    '------------------------
End Sub

'functions who calulates the monthly tech unav for LC owned alarms
'currently needs to be run directly from the Tech Unav sheet
Sub d_GetSumOfTechUnavAlarmsDataLConly(alarm_sheet_name As String)
    'Sheets(alarm_sheet_name).Select

    'defines which column numbers the months starts and ends. 7 = G 18 = R
    'G (7) H (8) I (9)osv... J K L M N O P Q R
    'in case there not are 12 months of data avalible then this needs to be modified.
    Dim monthStartColumn As Integer
    Dim monthEndColumn As Integer
    monthStartColumn = 7
    monthEndColumn = 18
    
    'loop though each month and sum tech unav for all owner groups in L&C, TurbineControl, SystemLevelControl and Environment
    For i = monthStartColumn To monthEndColumn
        
        'create range for the current month (this i)
        
        Dim month_range As String
        Dim current_column_Letter As String
        'need to load start and end row, THATS MISSING
        Dim alarm_row_start_numb As String
        Dim alarm_row_end_numb As String
        alarm_row_start_numb = Worksheets(alarm_sheet_name).Range("B8")
        alarm_row_end_numb = Worksheets(alarm_sheet_name).Range("B9")
        Debug.Print alarm_row_start_numb
        Debug.Print alarm_row_end_numb
        
        
        current_column_Letter = columnLetter(CLng(i))
        
        month_range = current_column_Letter & alarm_row_start_numb & ":" & current_column_Letter & alarm_row_end_numb
        
        Dim owner_range As String
        owner_range = "E" & alarm_row_start_numb & ":E" & alarm_row_end_numb
        
        Dim techUnav_sheet_name
        techUnav_sheet_name = alarm_sheet_name
        
        'variable to hold the amount of tech unav for each owner group for each month
        Dim techUnav_tc As Double
        Dim techUnav_slc As Double
        Dim techUnav_env As Double
        techUnav_tc = Application.WorksheetFunction.SumIf(Sheets(techUnav_sheet_name).Range(owner_range), "TurbineControl", Sheets(techUnav_sheet_name).Range(month_range))
        techUnav_slc = Application.WorksheetFunction.SumIf(Sheets(techUnav_sheet_name).Range(owner_range), "SystemLevelControl", Sheets(techUnav_sheet_name).Range(month_range))
        techUnav_env = Application.WorksheetFunction.SumIf(Sheets(techUnav_sheet_name).Range(owner_range), "Environment", Sheets(techUnav_sheet_name).Range(month_range))
        'techUnav_slc
        'Cells(10, columnLetter(CLng(i))) = Application.WorksheetFunction.SumIf(Sheets("Technical Unavailability").Range("E16:E333"), "TurbineControl", Sheets("Technical Unavailability").Range(month_range))
        'Cells(11, columnLetter(CLng(i))) = Application.WorksheetFunction.SumIf(Sheets("Technical Unavailability").Range("E16:E333"), "SystemLevelControl", Sheets("Technical Unavailability").Range(month_range))
        'Cells(12, columnLetter(CLng(i))) = Application.WorksheetFunction.SumIf(Sheets("Technical Unavailability").Range("E16:E333"), "Environment", Sheets("Technical Unavailability").Range(month_range))
        Dim calculate As Double
        calculate = techUnav_tc + techUnav_slc + techUnav_env
        
        'calculate = Cells(10, columnLetter(CLng(i))) + Cells(11, columnLetter(CLng(i))) + Cells(12, columnLetter(CLng(i)))
        Cells(67, columnLetter(CLng(i) - 5)) = calculate
        Dim dest_range As String
        dest_range = columnLetter(CLng(i) - 5) & 67
        Range(dest_range).Select
        Selection.NumberFormat = "0.00%"
        Selection.NumberFormat = "0.000%"
        Debug.Print calculate
        'calclate the above and put them in the cell
    Next i
    
    'Cells(9, "G") = ColumnLetter(18)
    'Cells(10, "G") = Application.WorksheetFunction.SumIf(Sheets("Technical Unavailability").Range("E16:E333"), "TurbineControl", Sheets("Technical Unavailability").Range("G16:G333"))
    'Cells(11, "G") = Application.WorksheetFunction.SumIf(Sheets("Technical Unavailability").Range("E16:E333"), "SystemLevelControl", Sheets("Technical Unavailability").Range("G16:G333"))
    'Cells(12, "G") = Application.WorksheetFunction.SumIf(Sheets("Technical Unavailability").Range("E16:E333"), "Environment", Sheets("Technical Unavailability").Range("G16:G333"))
    
End Sub

'Search the last month of the assigned alarm count sheet and calculated how many alarm belong to each owner group
Sub c_step_2_GetAllAlarmCountByOwner(a_start_row As Integer, a_end_row As Integer, a_sheetname As String)

    'in alarm count sheet
    Dim b_alarm_Start_Row As Integer
    Dim b_alarm_end_row As Integer
    Dim b_owner_column As String
    Dim b_last_month_column As String
    Dim b_lastMonthRange As String
    Dim b_ownerRange As String
    
    'get start and end row in the alarm count sheet
    b_alarm_Start_Row = Worksheets(a_sheetname).Range("B8")
    b_alarm_end_row = Worksheets(a_sheetname).Range("B9")
    'read settings from overview sheet
    b_last_month_column = Cells(13, "B")
    b_owner_column = Cells(16, "B")
    
    'createa ranges used for the formula based on the settings loaded above
    b_lastMonthRange = b_last_month_column & b_alarm_Start_Row & ":" & b_last_month_column & b_alarm_end_row
    b_ownerRange = b_owner_column & b_alarm_Start_Row & ":" & b_owner_column & b_alarm_end_row
    
    'in overview sheet

    
    
    'loop through the alarm owner list and calculate number of triggered alarmsd find how many alarms each owner group have been triggered for certain row (month)
    For i = a_start_row To a_end_row
        Cells(i, "B") = Application.WorksheetFunction.SumIf(Sheets(a_sheetname).Range(b_ownerRange), Range("A" & i), Sheets(a_sheetname).Range(b_lastMonthRange))
    Next i
   '-----------------------
   '--------alternative R1C1 style formula for the above, the start_row end_row variables not needed
    'Range("B19").Select
    'ActiveCell.FormulaR1C1 = _
        "=SUMIF(Sheet14!R16C5:R333C5,RC[-1],Sheet14!R16C18:R333C18)"
    'Range("B19").Select
    'Selection.AutoFill Destination:=Range("B19:B36"), Type:=xlFillDefault
    'Range("B19:B36").Select
    '------------------------
End Sub

'Search the last month of the assigned alarm count sheet and calculated how many alarm belong to each owner group
Sub c_step_x2x_GetSumOfAlarmsDataByGroup(a_start_row As Integer, a_end_row As Integer, a_sheetname As String, destinationColumn As String)

'in alarm count sheet
    Dim b_alarm_Start_Row As Integer
    Dim b_alarm_end_row As Integer
    Dim b_group_column As String
    Dim b_sub_group_column As String
    Dim b_last_month_column As String
    Dim b_lastMonthRange As String
    Dim b_group_range As String
    Dim b_sub_group_range As String
    
    'get start and end row in the alarm count sheet
    b_alarm_Start_Row = Worksheets(a_sheetname).Range("B8")
    b_alarm_end_row = Worksheets(a_sheetname).Range("B9")
    'read settings from overview sheet
    b_group_column = Cells(14, "B")
    b_sub_group_column = Cells(15, "B")
    b_last_month_column = Cells(13, "B")
    
    
    'create ranges used for the formula based on the settings loaded above
    'E16:E333
    b_lastMonthRange = b_last_month_column & b_alarm_Start_Row & ":" & b_last_month_column & b_alarm_end_row
    'C16:C333
    b_group_range = b_group_column & b_alarm_Start_Row & ":" & b_group_column & b_alarm_end_row
    'D16:333
    b_sub_group_range = b_sub_group_column & b_alarm_Start_Row & ":" & b_sub_group_column & b_alarm_end_row
    
    'loop through the alarm group list and calculate number of triggered alarms for each group
    For i = a_start_row To a_end_row
    
        Debug.Print i
        Cells(i, destinationColumn) = Application.WorksheetFunction.SumIfs(Sheets(a_sheetname).Range(b_lastMonthRange), Sheets(a_sheetname).Range(b_group_range), Range("A" & i), Sheets(a_sheetname).Range(b_sub_group_range), Range("B" & i))
    Next i

   '-----------------------
   '--------alternative R1C1 style formula for the above, the start_row end_row variables not needed

    'Range("C40").Select
    'ActiveCell.FormulaR1C1 = _
        "=SUMIFS(Sheet14!R16C18:R333C18,Sheet14!R16C3:R333C3,RC[-2],Sheet14!R16C4:R333C4,RC[-1])"
    'Range("C40").Select
    'Selection.AutoFill Destination:=Range("C40:C84"), Type:=xlFillDefault
    'Range("C40:C84").Select
End Sub


'Search the last month of the assigned alarm count sheet and calculated how many alarm belong to each owner group
Sub c_step_2_GetAllAlarmTechUnavByOwner(a_start_row As Integer, a_end_row As Integer, a_sheetname As String)

    'in alarm count sheet
    Dim b_alarm_Start_Row As Integer
    Dim b_alarm_end_row As Integer
    Dim b_owner_column As String
    Dim b_last_month_column As String
    Dim b_lastMonthRange As String
    Dim b_ownerRange As String
    
    'get start and end row in the alarm count sheet
    b_alarm_Start_Row = Worksheets(a_sheetname).Range("B8")
    b_alarm_end_row = Worksheets(a_sheetname).Range("B9")
    'read settings from overview sheet
    b_last_month_column = Cells(13, "B")
    b_owner_column = Cells(16, "B")
    
    'createa ranges used for the formula based on the settings loaded above
    b_lastMonthRange = b_last_month_column & b_alarm_Start_Row & ":" & b_last_month_column & b_alarm_end_row
    b_ownerRange = b_owner_column & b_alarm_Start_Row & ":" & b_owner_column & b_alarm_end_row
    
    'in overview sheet

    
    
    'loop through the alarm owner list and calculate number of triggered alarmsd find how many alarms each owner group have been triggered for certain row (month)
    For i = a_start_row To a_end_row
        Cells(i, "C") = Application.WorksheetFunction.SumIf(Sheets(a_sheetname).Range(b_ownerRange), Range("A" & i), Sheets(a_sheetname).Range(b_lastMonthRange))
    Next i
   '-----------------------
   '--------alternative R1C1 style formula for the above, the start_row end_row variables not needed
    'Range("B19").Select
    'ActiveCell.FormulaR1C1 = _
        "=SUMIF(Sheet14!R16C5:R333C5,RC[-1],Sheet14!R16C18:R333C18)"
    'Range("B19").Select
    'Selection.AutoFill Destination:=Range("B19:B36"), Type:=xlFillDefault
    'Range("B19:B36").Select
    '------------------------
End Sub
    
  

'Search the last month of the assigned alarm count sheet and calculated how many alarm belong to each group/sub group
Sub c_step_3_GetAllAlarmCountByGroup_and_subgroup(a_start_row As Integer, a_end_row As Integer, a_sheetname As String)

'in alarm count sheet
    Dim b_alarm_Start_Row As Integer
    Dim b_alarm_end_row As Integer
    Dim b_group_column As String
    Dim b_sub_group_column As String
    Dim b_last_month_column As String
    Dim b_lastMonthRange As String
    Dim b_group_range As String
    Dim b_sub_group_range As String
    
    'get start and end row in the alarm count sheet
    b_alarm_Start_Row = Worksheets(a_sheetname).Range("B8")
    b_alarm_end_row = Worksheets(a_sheetname).Range("B9")
    'read settings from overview sheet
    b_group_column = Cells(14, "B")
    b_sub_group_column = Cells(15, "B")
    b_last_month_column = Cells(13, "B")
    
    
    'create ranges used for the formula based on the settings loaded above
    'E16:E333
    b_lastMonthRange = b_last_month_column & b_alarm_Start_Row & ":" & b_last_month_column & b_alarm_end_row
    'C16:C333
    b_group_range = b_group_column & b_alarm_Start_Row & ":" & b_group_column & b_alarm_end_row
    'D16:333
    b_sub_group_range = b_sub_group_column & b_alarm_Start_Row & ":" & b_sub_group_column & b_alarm_end_row
    
    'loop through the alarm group list and calculate number of triggered alarms for each group
    For i = a_start_row To a_end_row
    
        Debug.Print i
        Cells(i, "C") = Application.WorksheetFunction.SumIfs(Sheets(a_sheetname).Range(b_lastMonthRange), Sheets(a_sheetname).Range(b_group_range), Range("A" & i), Sheets(a_sheetname).Range(b_sub_group_range), Range("B" & i))
    Next i

   '-----------------------
   '--------alternative R1C1 style formula for the above, the start_row end_row variables not needed

    'Range("C40").Select
    'ActiveCell.FormulaR1C1 = _
        "=SUMIFS(Sheet14!R16C18:R333C18,Sheet14!R16C3:R333C3,RC[-2],Sheet14!R16C4:R333C4,RC[-1])"
    'Range("C40").Select
    'Selection.AutoFill Destination:=Range("C40:C84"), Type:=xlFillDefault
    'Range("C40:C84").Select
End Sub
'Search the last month of the assigned alarm count sheet and calculated how many alarm belong to each group/sub group
Sub c_step_3_GetAllAlarmTechUnavByGroup_and_subgroup(a_start_row As Integer, a_end_row As Integer, a_sheetname As String)

'in alarm count sheet
    Dim b_alarm_Start_Row As Integer
    Dim b_alarm_end_row As Integer
    Dim b_group_column As String
    Dim b_sub_group_column As String
    Dim b_last_month_column As String
    Dim b_lastMonthRange As String
    Dim b_group_range As String
    Dim b_sub_group_range As String
    
    'get start and end row in the alarm count sheet
    b_alarm_Start_Row = Worksheets(a_sheetname).Range("B8")
    b_alarm_end_row = Worksheets(a_sheetname).Range("B9")
    'read settings from overview sheet
    b_group_column = Cells(14, "B")
    b_sub_group_column = Cells(15, "B")
    b_last_month_column = Cells(13, "B")
    
    
    'create ranges used for the formula based on the settings loaded above
    'E16:E333
    b_lastMonthRange = b_last_month_column & b_alarm_Start_Row & ":" & b_last_month_column & b_alarm_end_row
    'C16:C333
    b_group_range = b_group_column & b_alarm_Start_Row & ":" & b_group_column & b_alarm_end_row
    'D16:333
    b_sub_group_range = b_sub_group_column & b_alarm_Start_Row & ":" & b_sub_group_column & b_alarm_end_row
    
    'loop through the alarm group list and calculate number of triggered alarms for each group
    For i = a_start_row To a_end_row
    
        Debug.Print i
        Cells(i, "d") = Application.WorksheetFunction.SumIfs(Sheets(a_sheetname).Range(b_lastMonthRange), Sheets(a_sheetname).Range(b_group_range), Range("A" & i), Sheets(a_sheetname).Range(b_sub_group_range), Range("B" & i))
    Next i

   '-----------------------
   '--------alternative R1C1 style formula for the above, the start_row end_row variables not needed

    'Range("C40").Select
    'ActiveCell.FormulaR1C1 = _
        "=SUMIFS(Sheet14!R16C18:R333C18,Sheet14!R16C3:R333C3,RC[-2],Sheet14!R16C4:R333C4,RC[-1])"
    'Range("C40").Select
    'Selection.AutoFill Destination:=Range("C40:C84"), Type:=xlFillDefault
    'Range("C40:C84").Select
End Sub



'get top 10 for alarms with higest % of tech unavailibility for all owners for the alarm overview
Sub c_step_4_Get_top_10_by_alarm_tech_unav(startRow As Integer, endRow As Integer, tech_unav_sheet_name As String)
    Dim alarm_overview_sheet_name
    'get sheet name for alarmoverview
    alarm_overview_sheet_name = ActiveSheet.Name
    
    'go to tech unav sheet and filter the last month so highest are in the top of the table
    Sheets(tech_unav_sheet_name).Select
       ActiveWorkbook.Worksheets(tech_unav_sheet_name).AutoFilter.Sort.SortFields.Clear
       ActiveWorkbook.Worksheets(tech_unav_sheet_name).AutoFilter.Sort.SortFields.Add2 Key:= _
           Range("R15"), SortOn:=xlSortOnValues, Order:=xlDescending, DataOption:= _
           xlSortNormal
       With ActiveWorkbook.Worksheets(tech_unav_sheet_name).AutoFilter.Sort
           .Header = xlYes
           .MatchCase = False
           .Orientation = xlTopToBottom
           .SortMethod = xlPinYin
           .Apply
    End With
    
    
    'select first 10 rows and headings and copy them back to the alarm overview sheet
    Range("A15:E25").Select
    Selection.Copy
    Sheets(alarm_overview_sheet_name).Select
    Range("A" & startRow).Select
    ActiveSheet.Paste
    
    'copy the tech unav numbers for the first 10 rows to the alarm overview sheet
    Sheets(tech_unav_sheet_name).Select
    Range("R16:R25").Select
    Selection.Copy
    Sheets(alarm_overview_sheet_name).Select
    Range("F" & startRow + 1).Select
    ActiveSheet.Paste
    'format numbers to 3 deceimals
    Selection.NumberFormat = "0.000%"
    
    'add heading to tech unav column
    Cells(startRow, "F") = "Technical Unavailability"
    
    
   
End Sub

'get top 10 for most triggered alarms for all owners for the alarm overview
Sub c_step_4_Get_top_10_by_alarm_count(startRow As Integer, endRow As Integer, alarm_count_sheet_name As String)
    Dim alarm_overview_sheet_name
     'get sheet name for alarmoverview
    alarm_overview_sheet_name = ActiveSheet.Name
    
    'go to alarm count sheet and filter the last month so highest are in the top of the table
    Sheets(alarm_count_sheet_name).Select
       ActiveWorkbook.Worksheets(alarm_count_sheet_name).AutoFilter.Sort.SortFields.Clear
       ActiveWorkbook.Worksheets(alarm_count_sheet_name).AutoFilter.Sort.SortFields.Add2 Key:= _
           Range("R15"), SortOn:=xlSortOnValues, Order:=xlDescending, DataOption:= _
           xlSortNormal
       With ActiveWorkbook.Worksheets(alarm_count_sheet_name).AutoFilter.Sort
           .Header = xlYes
           .MatchCase = False
           .Orientation = xlTopToBottom
           .SortMethod = xlPinYin
           .Apply
    End With
    
    
    'select first 10 rows and headings and copy them back to the alarm overview sheet
    Range("A15:E25").Select
    Selection.Copy
    Sheets(alarm_overview_sheet_name).Select
    Range("A" & startRow).Select
    ActiveSheet.Paste
    
    'copy the alarm count numbers for the first 10 rows to the alarm overview sheet
    Sheets(alarm_count_sheet_name).Select
    Range("R16:R25").Select
    Selection.Copy
    Sheets(alarm_overview_sheet_name).Select
    Range("F" & startRow + 1).Select
    ActiveSheet.Paste
    
    'add heading to alarm count column
    Cells(startRow, "F") = "Alarm Count"
End Sub

Private Sub Set_3_Decimals_on_range(rng As String)
    'Get range for 12 months in the tech unav sheet
    Range(rng).Select
    'Set numb of decimals to 3
    Selection.NumberFormat = "0.000%"
End Sub

Private Sub ChangeRangeToPercentage(rng As String)
'
' ChangeToPercentage Macro
'

'
    Range(rng).Select
    Selection.NumberFormat = "0.000%"
    
End Sub



Function c_AlarmOverview_create_final_Headlines()
'
' AlarmOverview_createHeadlines Macro
'

'
    Range("A1:E16").Select
    Selection.ClearContents
    Range("A6:C7").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 10655744
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    Selection.Font.Size = 12
    Selection.Font.Size = 14
    Selection.Font.Size = 16
    Selection.Font.Size = 14
    With Selection.Font
        .Name = "Calibri"
        .Size = 14
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
   
    With Selection.Font
        .Name = "Calibri"
        .Size = 22
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    Range("A8:D9").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Font
        .Color = -6121472
        .TintAndShade = 0
    End With
'    ActiveCell.FormulaR1C1 = "Month XX Year XX Overview" "Alarm Report " & Worksheets("Sheet1").Range("E1")
    ActiveCell.FormulaR1C1 = "All alarms overview last month (" & Worksheets("Sheet6").Range("R15") & ")"
    Range("A8:D9").Select
    With Selection.Font
        .Name = "Calibri"
        .Size = 22
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .Color = -6121472
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    Range("A10:D11").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Font
        .Name = "Calibri"
        .Size = 22
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0.499984740745262
    End With
    ActiveCell.FormulaR1C1 = "Reported turbines: " & Worksheets("Sheet6").Range("R14")
    Range("A12:B12").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Font
        .Color = -6121472
        .TintAndShade = 0
    End With
    ActiveCell.FormulaR1C1 = "Total Triggered Alarms"
    Range("A13:B13").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    ActiveCell.FormulaR1C1 = "Total technical unavailability"
    Range("A12:B12").Select
    Selection.Font.Size = 12
    Selection.Font.Size = 14
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    Range("A13:B13").Select
    With Selection.Font
        .Name = "Calibri"
        .Size = 14
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    With Selection.Font
        .Color = -6121472
        .TintAndShade = 0
    End With
    Range("C12").Select
    Application.CutCopyMode = False
    
    Range("C13").Select
    Application.CutCopyMode = False
    
    Range("C14").Select
    ActiveWindow.SmallScroll Down:=-12
    Range("A1").Select
    '
    Range("A1").Select
    ActiveSheet.Pictures.Insert( _
        "https://siemensgamesa.sharepoint.com/teams/OG00570/Product/TurbineAlarms/L&C%20Alarm%20Reports/Resources/SGRElogo.png" _
        ).Select
    Selection.ShapeRange.ScaleWidth 0.5492424242, msoFalse, msoScaleFromTopLeft
    Selection.ShapeRange.ScaleHeight 0.5492422033, msoFalse, msoScaleFromTopLeft
    Cells(12, "C") = Application.WorksheetFunction.Sum(Range("B19:B36"))
    Cells(13, "C") = Application.WorksheetFunction.Sum(Range("C19:C36"))
    Range("C13").Select
    Selection.NumberFormat = "0.00%"
     Range("A16:C17").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Font
        .Name = "Calibri"
        .Size = 14
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    ActiveCell.FormulaR1C1 = "Alarm stats by owner group"
    Range("A16:C17").Select
    With Selection.Font
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0.499984740745262
    End With
    Selection.Font.Size = 16
    ActiveWindow.SmallScroll Down:=9
    Range("A38:D39").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Font
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0.499984740745262
    End With
    With Selection.Font
        .Name = "Calibri"
        .Size = 16
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0.499984740745262
        .ThemeFont = xlThemeFontMinor
    End With
    ActiveCell.FormulaR1C1 = "Alarm stats by system (Group / sub group)"
    Range("A6:D7").Select
    'get turbine type from the pivot table sheet
    ActiveCell.FormulaR1C1 = "Alarm Report " & Worksheets("Sheet1").Range("E1")
    Range("A8:D9").Select
    Cells.Select
    Cells.EntireColumn.AutoFit
    ActiveWindow.DisplayGridlines = False
    
    
    'NEWLY ADDED
     Range("A6:D11").Select
    Application.CutCopyMode = False
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
    End With
    Range("A16:C17").Select
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    ActiveWindow.SmallScroll Down:=15
    Range("A38:D39").Select
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    ActiveWindow.SmallScroll Down:=51
    
    Range("A88:F89").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    ActiveCell.FormulaR1C1 = "Top 10 Alarms By Technical Unavailability All Owners"
    Range("A104:F105").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    ActiveCell.FormulaR1C1 = "Top 10 Alarms By Alarm Count All Owners"
    Range("A104").Select
   
    Range("A6:D7").Select
    Selection.Copy
    ActiveWindow.SmallScroll Down:=66
    Range("A88:F89").Select
    Selection.PasteSpecial Paste:=xlPasteFormats, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    Application.CutCopyMode = False
    Selection.Copy
    Range("A104:F105").Select
    Selection.PasteSpecial Paste:=xlPasteFormats, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    Application.CutCopyMode = False
    ActiveWindow.SmallScroll Down:=-51
    Selection.Copy
    Range("A38:D39").Select
    Selection.PasteSpecial Paste:=xlPasteFormats, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    Application.CutCopyMode = False
    ActiveWindow.SmallScroll Down:=-21
    Selection.Copy
    Range("A16:C17").Select
    Selection.PasteSpecial Paste:=xlPasteFormats, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    Application.CutCopyMode = False


    
End Function





'function to get top 10 L&C alarms for both alarm count and tech unav
Sub d_Get_Top_10_LC_Alarms(alarm_sheet_name As String, rng As String, col_letter As String, dest_sheet_name As String, dest_range As String)

    'select the alarm sheet name
    Sheets(alarm_sheet_name).Select
    'filter the table so it only shows L&C related groups
    ActiveSheet.Range("$A$" & Cells(8, "B") & ":$R$" & Cells(9, "B")).AutoFilter Field:=5, Criteria1:=Array( _
        "Environment", "SystemLevelControl", "TurbineControl"), Operator:= _
        xlFilterValues
    'Sort so the higest values are in the top of the latest month
    ActiveWorkbook.Worksheets(alarm_sheet_name).AutoFilter.Sort.SortFields.Clear
    ActiveWorkbook.Worksheets(alarm_sheet_name).AutoFilter.Sort.SortFields.Add2 Key:= _
        Range("R" & Cells(8, "B") & ":R" & Cells(9, "B")), SortOn:=xlSortOnValues, Order:=xlDescending, DataOption _
        :=xlSortNormal
    With ActiveWorkbook.Worksheets(alarm_sheet_name).AutoFilter.Sort
        .Header = xlYes
        .MatchCase = False
        .Orientation = xlTopToBottom
        .SortMethod = xlPinYin
        .Apply
    End With
    
    'select first 10 rows in the filtered list for the last month and copy it to the destination sheet and range
    Dim i As Long
    Dim r As Range
    Dim rwC As Range
    Set r = Range(rng, Range(col_letter & Rows.Count).End(xlUp)).SpecialCells(12)
    For Each rwC In r
     i = i + 1
     '11 gives us the header + the first 10 alarms
     If i = 11 Or i = r.Count Then Exit For
    Next rwC
    Range(r(1), rwC).SpecialCells(12).Copy
    
    Sheets(dest_sheet_name).Select
    Range(dest_range).Select
    ActiveSheet.Paste
    
End Sub

Private Sub d_LC_view_style_page()
'
' MarcoLCviewStyle Macro
'

'
    
    Range("A6:D7").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    With Selection.Font
        .Name = "Calibri"
        .Size = 18
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 10655744
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    Range("A8:D9").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    With Selection.Font
        .Name = "Calibri"
        .Size = 18
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 10655744
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    Range("A12:C12").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Font
        .Color = -6121472
        .TintAndShade = 0
    End With
    With Selection.Font
        .Name = "Calibri"
        .Size = 16
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .Color = -6121472
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection.Font
        .Name = "Calibri"
        .Size = 12
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .Color = -6121472
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    Range("A13:C13").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Font
        .Color = -6121472
        .TintAndShade = 0
    End With
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    With Selection.Font
        .Name = "Calibri"
        .Size = 12
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .Color = -6121472
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    Range("A18:F19").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 10655744
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    With Selection.Font
        .Name = "Calibri"
        .Size = 18
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    Range("A23:D24").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 10655744
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    With Selection.Font
        .Name = "Calibri"
        .Size = 18
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    Range("E23:F24").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 10655744
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    ActiveWindow.SmallScroll Down:=18
    Range("A40:D41").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 10655744
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    With Selection.Font
        .Name = "Calibri"
        .Size = 18
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    Range("E40:F41").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 10655744
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    ActiveWindow.SmallScroll Down:=24
    Range("A64:M65").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    With Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 10655744
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    With Selection.Font
        .Name = "Calibri"
        .Size = 18
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    Range("A66:A68").Select
    With Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 10655744
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    With Selection.Font
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    
    Range("A1").Select
    ActiveSheet.Pictures.Insert( _
        "https://siemensgamesa.sharepoint.com/teams/OG00570/Product/TurbineAlarms/L&C%20Alarm%20Reports/Resources/SGRElogo.png" _
        ).Select
    Selection.ShapeRange.ScaleWidth 0.4242424242, msoFalse, msoScaleFromTopLeft
    Selection.ShapeRange.ScaleHeight 0.4242424242, msoFalse, msoScaleFromTopLeft
End Sub


'convert the entire alarm range from text to number, so we can sort it from smallest to largest in order to get all wtc3 alarms first
Function del_wtc3_alarms_convert_entire_alarm_range_from_text_to_number_and_sort()
'
' Macro1 Macro
'

'
    Range("A16").Select
    Range(Selection, Selection.End(xlDown)).Select
    
    Dim entire_alarm_range As String
    entire_alarm_range = Selection.Address
    
    Range(entire_alarm_range) = Range(entire_alarm_range).Value
    Application.CutCopyMode = False
    ActiveWorkbook.Worksheets(ActiveSheet.Name).AutoFilter.Sort.SortFields.Clear
    ActiveWorkbook.Worksheets(ActiveSheet.Name).AutoFilter.Sort.SortFields.Add2 Key:= _
        Range("A15"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:= _
        xlSortNormal
    With ActiveWorkbook.Worksheets(ActiveSheet.Name).AutoFilter.Sort
        .Header = xlYes
        .MatchCase = False
        .Orientation = xlTopToBottom
        .SortMethod = xlPinYin
        .Apply
    End With
End Function

'if we have below 6 digits, we know we are dealing with a wtc3 alarm and it can be safely deleted
Function del_wtc3_alarms_check_if_cell_have_below_6_digits_if_not_delete_row()
    Dim temp_val As String
    Dim deleted_alarms As String

    For i = Cells(8, "B") To Cells(9, "B")
        temp_val = Cells(i, "A")
        'if length is lesss than 6 and bigger than 0 then we want to delete the alarm since it a WTC3 alarm (3,4 or 5 digits)
        If Len(temp_val) < 6 And Len(temp_val) > 0 Then
            'Debug.Print "Length = " & Len(temp_val)
            'Debug.Print "Delete it"
            'Select the current row
            deleted_alarms = deleted_alarms & temp_val & ", "
            Rows(i & ":" & i).Select
            'delete the row
            Selection.Delete Shift:=xlUp
            i = i - 1
        ElseIf Len(temp_val) = 0 Then
            Exit Function
        Else
            'Debug.Print "Length = " & Len(temp_val)
            'Debug.Print "Keep it"
        End If
    Next i
    Cells(4, "D") = "Deleted WTC alarms"
    Cells(4, "E") = deleted_alarms
End Function

'check if cells in range are blank
Function allBlank(rg As Range) As Boolean
    allBlank = (rg.Cells.Count = WorksheetFunction.CountBlank(rg))
End Function




'find all alarms with all empty cells in the months columns and delete it
Function Delete_empty_Rows_from_data_set()
    Dim deleted_alarms As String
    Dim rows_to_be_deleted As String
    Set alarms_to_be_deleted_arrary = CreateObject("System.Collections.ArrayList")
    
    'lopp thorugh all alarm in current sheet and find alarms with all empty cells in the month columns
    For i = Cells(8, "B") To Cells(9, "B")
    'if all cells are blank we want to delete it
        If allBlank(Range("G" & i & ":R" & i)) Then
            'Debug.Print "all cells are empty! " & "i = " & i
            deleted_alarms = deleted_alarms & Cells(i, "A") & " ,"
            rows_to_be_deleted = rows_to_be_deleted & i & " ,"
            'add the current alarm to the alarm arrary
            alarms_to_be_deleted_arrary.Add Cells(i, "A")
           
            
        Else
        '...
        End If
    Next i
    'Debug.Print "deleted alarms: " & deleted_alarms
    'Debug.Print "rows to be deleted: " & rows_to_be_deleted
   
    
    Dim alarm As Variant
    Dim counter As Integer
    counter = 0
    'loop through alarms with empty data and delete the given alarm
    For Each alarm In alarms_to_be_deleted_arrary
        counter = counter + 1
        Dim row_numb As String
        
        'find the alarm
        Cells.Find(What:=alarm, After:=ActiveCell, LookIn:=xlFormulas, LookAt _
            :=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, MatchCase:= _
            False, SearchFormat:=False).Activate
        'get the alarm row numb
        row_numb = ActiveCell.Row
        'select the given row
        If row_numb > 15 Then
            Rows(row_numb & ":" & row_numb).Select
            Selection.Delete Shift:=xlUp
        End If
        
        'Debug.Print alarm
    Next
    Debug.Print "totals alarms with empty data to be deleted: counter = " & counter
    Cells(3, "D") = "Deleted alarms with all cells empty. Total: " & counter
    Cells(3, "E") = deleted_alarms
    'updaete end rows since we deleted some
    Cells(9, "B") = LastRowInOneColumn("a")
End Function


'renames all shets for final report
Function RenameSheets()
'
' Macro1 Macro
'
'
    'Sheets("Sheet1").Select
    'ActiveWindow.SelectedSheets.Delete

    Sheets("Sheet6").Name = "Technical Unavailability"
    Sheets("Sheet5").Name = "Alarm Count"
    Sheets("Sheet7").Name = "Alarm Overview Last Month"
    Sheets("Sheet8").Name = "L&C Alarm Overview"
    Sheets("Sheet9").Name = "Top10LC alarm_site_distribution"
    Sheets("Sheet10").Name = "Top10LC alarm_turb_distribution"
    
End Function



Sub Format_table_based_on_range()
'
' Macro5 Macro
'

'
    Dim tableName
    tableName = "Table" & Int((999 - 100 + 1) * Rnd + 100)
    Debug.Print tableName
    Range("A15").Select
    Range(Selection, Selection.End(xlToRight)).Select
    Range(Selection, Selection.End(xlDown)).Select
    Application.CutCopyMode = False
    ActiveSheet.ListObjects.Add(xlSrcRange, Range("$A$15:$S$122"), , xlYes).Name = _
        tableName
    Range(tableName & "[#All]").Select
    ActiveSheet.ListObjects(tableName).TableStyle = "TableStyleMedium2"
End Sub



'function which changes a column width
Sub Change_column_width(sheetname As String, newWidth As String, affectedColumn As String)
    With Worksheets(sheetname).Columns(affectedColumn)
     .ColumnWidth = newWidth
    End With
End Sub

'changes widt for description cells and changes column widt of needed columns
Function style_cells_and_row_for_alarmcount_and_tech_unav_sheets()
    'change col b column width
    Columns("B:B").Select
    Selection.ColumnWidth = 31.14
    'Select entire col b and wrap text
    Range("B15:B" & HelperModule.LastRowInOneColumn("b")).Select
    With Selection
        .VerticalAlignment = xlBottom
        .WrapText = True
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    'change col width for col f
    Columns("F:F").Select
    Selection.ColumnWidth = 11.71
    'wraptext for reported turbines field
    Range("F14").Select
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = True
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Columns("G:R").Select
    Selection.ColumnWidth = 8.5
End Function

'delete 12 month trendline and re add the column again
'format data as table
'used on alarm count and tech unav sheets
Function format_alarm_count_and_tech_unav_sheets()
   'delete 12 month trendline and re add the column again
    Columns("S:S").Select
    Selection.Delete Shift:=xlToLeft
    Range("A1").Select
    
    'format the data as table
    Call HelperModule.format_range_as_table("A15:R" & HelperModule.LastRowInOneColumn("a"), 2)
    Range("S15") = "12 Month Trendline"
End Function





