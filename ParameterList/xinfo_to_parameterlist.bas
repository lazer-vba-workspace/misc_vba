Attribute VB_Name = "xinfo_to_parameterlist"

'to run this method we need to know
'we have all correct typeselections in the "lasttypeselections" sheet in the excel helper sheets file
'so we need to have checked that we have run method
'a, b and c before running this complete method
Sub parList_1_run_generate_list()
    Call parList_b_Copy_ParOwner_Sheet_and_LastTypeSlection_From_Helper_sheets_file
    Call parList_d_LastTypeSelections_set_filter_based_on_list
    Call parList_e_copy_parameterlist_after_settings_types
    Call parList_f_UpdateParameterOwnerOwner
    Sheets("ParameterList").Select
    Call HelperModule.format_range_as_table("A7:H" & HelperModule.LastRowInOneColumn("a"), 1)
    Call parList_q_Add_new_columns_to_standard_parameterlist
    Call parList_qqq_CopyHeadersForRegularParList
    
End Sub

'from a new xinfo file, find all main type selections and add them to a new sheet
Sub parList_a_Get_All_Type_Selections_From_Xinfo()
'
' Macro1 Macro
'

'
    
    'get all typeselections
    Sheets("Parameters").Select
    
    'filter to only show all parameters with a typeselection
    ActiveSheet.Range("$B$4:$M$" & HelperModule.LastRowInOneColumn("B")).AutoFilter Field:=2, Criteria1:="<>"
    'select the entire row
    Range("C5").Select
    Range(Selection, Selection.End(xlDown)).Select
    'copy
    Selection.Copy
    'add new sheet and paste values
    Sheets.Add After:=ActiveSheet
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    'remove all duplicates, so we only have all uniquie type selections
    ActiveSheet.Range("$A$1:$A$" & LastRowInOneColumn("A")).RemoveDuplicates Columns:=1, Header:= _
        xlNo
    
    'get all main type selections, meaning all types without a ":" and show them
    Range("A1").Select
    Selection.AutoFilter
    ActiveSheet.Range("$A$1:$A$" & LastRowInOneColumn("A")).AutoFilter Field:=1, Criteria1:="<>*:*", _
        Operator:=xlAnd
    'rename sheet
    ActiveSheet.Name = "AllTypeSelections"
End Sub





'copies the parOwner and lasttypeselection helper sheets to this file
Sub parList_b_Copy_ParOwner_Sheet_and_LastTypeSlection_From_Helper_sheets_file()

Dim CurrentFileName As String
CurrentFileName = ThisWorkbook.Name
    Workbooks.Open Filename:= _
        "https://siemensgamesa-my.sharepoint.com/personal/lars_mathiesen_siemensgamesa_com/Documents/temp/12_excel_helpers/helper_sheets.xlsx" _
        , UpdateLinks:=0
    Sheets("LastTypeselections").Select
    Sheets("LastTypeselections").Copy Before:=Workbooks( _
        CurrentFileName).Sheets(1)
    Windows("helper_sheets.xlsx").Activate
    Sheets("ParameterOwnerUpdater").Select
    Sheets("ParameterOwnerUpdater").Copy Before:=Workbooks( _
        CurrentFileName).Sheets(1)
        
    Windows("helper_sheets.xlsx").Activate
    ActiveWindow.Close
End Sub


'copies all main type type selctions from the xinfo we generate on the sheet "AllTypeSelections" and all types from the last type selections sheet
'now all you have to do is mark column A and C and make conditional formating with Highlight duplicates
'now you see the ones with no fill in column A or C are the ones we are missing for the given turbine to find.
'column d will contain the main type selection types we are missing
'find the missing ones and add it to the "lasttypeselctions" sheet
Sub parList_c_Compare_Last_Typeselections_with_xinfo()
'
'

    Dim sheetName As String
    'go to alltypeselections and copy all unique parametesr depend types
    Sheets("AllTypeSelections").Select
    'set the name of the new sheet we will do our comparison
    Range("A1:A" & HelperModule.LastRowInOneColumn("a")).Select
    Selection.Copy
    
    sheetName = "TypeSelectionCompare"
    'add new sheet
    Sheets.Add After:=ActiveSheet
    'rename the new sheet
    ActiveSheet.Name = sheetName
    'select the cell we want to paste our data into
    Range("A2").Select
    'paste values only
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    'Range("A1").Select
    'Application.CutCopyMode = False
   
    Range("B2").Select
    'go to the lasttypeselections sheet
    Sheets("LastTypeselections").Select
    'select the first occurance of a typeselection
    Range("A7").Select
    'select to end
    Range(Selection, Selection.End(xlDown)).Select
    'copy
    Selection.Copy
    'go back to the comparison sheet
    Sheets(sheetName).Select
    'paste
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    
    'the below are removing everything in the typeselections we just copied from "lasttypeselection"
    'after the ":" in the given type - so we can do a quick comparison which new types we might be missing.
    
    Dim lastRowNumb As Integer
    lastRowNumb = HelperModule.LastRowInOneColumn("B")
    
    Dim currentCellRange As String
    Dim currentCellVal As String
    Dim destinationCellRange As String
    
    Dim I As Integer
    
    'loop through all type selections
    For I = 2 To lastRowNumb
        currentCellRange = "B" & I
        destinationCellRange = "C" & I
        currentCellVal = Range(currentCellRange)
        'if we meet a cell ifout a ":" then the code breaks, we know the below will come
        'that why the if else statement
        If currentCellVal = "=" Then
        
        Else
        'remove everything after the ":" in the given type selection
        Range(destinationCellRange) = Left(currentCellVal, InStr(currentCellVal, ":") - 1)
        
        End If
        'reset our variables so we are ready for the next round
        currentCellRange = ""
        destinationCellRange = ""
    Next I
    
    'set headings
    Range("A1") = "All Types from xinfo"
    Range("B1") = "LastTypeselections"
    Range("C1") = "removed all after (:) in column b"
    Range("D1") = "all missing type. if empty we are good"
    Range("E1") = "now you need to mark column a and column c and make condiditional formating and highligt duplicate cells. Then we have no fill in the type selections we are missing "
    
    'fit cells to text is proper visible
    Cells.Select
    Cells.EntireColumn.AutoFit
    
    'select all types in col "A" and col "C"
    lastRowColA = HelperModule.LastRowInOneColumn("a")
    Range("A2:A" & lastRowColA & ",C2:C" & lastRowNumb).Select
    'make conditional formatting and mark duplicate values with light red
    Selection.FormatConditions.AddUniqueValues
    Selection.FormatConditions(Selection.FormatConditions.Count).SetFirstPriority
    Selection.FormatConditions(1).DupeUnique = xlDuplicate
    With Selection.FormatConditions(1).Font
        .Color = -16383844
        .TintAndShade = 0
    End With
    With Selection.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .Color = 13551615
        .TintAndShade = 0
    End With
    Selection.FormatConditions(1).StopIfTrue = False
    
    
    find_all_type_selection_cells_with_no_fill
    
End Sub


'functions who check entire row a, and find all type selection which have no fill, meaning they
'are missing the the last typeseltions list
Function find_all_type_selection_cells_with_no_fill()
    
    Dim I As Integer
    Dim numb_of_matches
    numb_of_matches = 0
    endNumb = LastRowInOneColumn("a")
    
    For I = 2 To endNumb

        If Range("A" & I).DisplayFormat.Interior.Color = "16777215" Then
        
            numb_of_matches = numb_of_matches + 1
            Range("D" & numb_of_matches + 1) = Range("a" & I)
        
        End If
    
    Next I
    
End Function


'Documen can be found here'
'C:\Users\mathi01l\OneDrive - SIEMENSGAMESA\temp\excel_helpers\setFiltersBasedOnList.xlsm'
'C:\Users\mathi01l\OneDrive - SIEMENSGAMESA\temp\excel_helpers\helper_sheets.xlsx'
Sub parList_d_LastTypeSelections_set_filter_based_on_list()
'
' filterfilterfilter Macro

'
Sheets("LastTypeselections").Select

Dim parSheetName As String
Dim topLeftParameterSheetCellNumb As String
Dim typeSelectionRange
'Set parameter list sheet name'
parSheetName = Cells(1, "B")
Debug.Print "read B:1 as " + parSheetName
'Set topLeftParameterSheetCellNumb to top left cell number in the parameter sheet '
topLeftParameterSheetCellNumb = Cells(2, "B")
Debug.Print "read B:2 as " + topLeftParameterSheetCellNumb

'Set type selection range
typeSelectionRange = Cells(3, "B")
Debug.Print "read B:3 as " + typeSelectionRange


'below can be used to set the range by a inputbox'
'----'
'typeSelectionRange = InputBox("Assign range etc. A1:A25", "Set Range", "A4:A42")'
'----'

'Debug.Print typeSelectionRange'

'Sheets("LastTypeSelections").Select'
Dim ara As Variant
ara = Range(typeSelectionRange)

Sheets(parSheetName).Select

    Range("B4").Select
    Range(Selection, Selection.End(xlToRight)).Select
    Range(Selection, Selection.End(xlDown)).Select
    Dim parameterListRange As String
    parameterListRange = Selection.Address
    Debug.Print "Range is ....... " + parameterListRange
    
    ara = Application.Transpose(ara)
    ara = Split(Join(ara, ","), ",")
    ActiveSheet.Range(parameterListRange).AutoFilter Field:=2, Criteria1:=ara, Operator:=xlFilterValues

End Sub

'copying the parameter after filter of type selections are applied to a new sheet and renaming it parmaeter list
Sub parList_e_copy_parameterlist_after_settings_types()
Sheets("Parameters").Select
Dim lastrow_col_b As String

'find the last row of the parmaeters sheet
lastrow_col_b = HelperModule.LastRowInOneColumn("b")

'select the entire table
Range("B4:J" & lastrow_col_b).Select

Selection.Copy

'a neww sheet and paste special values only
Sheets.Add After:=ActiveSheet
    Range("A7").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    'delete col g, contaning "chgAppl"
    Columns("G:G").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
Sheets(ActiveSheet.Name).Name = "ParameterList"

Debug.Print lastrow_col_b
Debug.Print lastrow_col_j
End Sub


'document can be found at the following path'
'C:\Users\mathi01l\OneDrive - SIEMENSGAMESA\temp\excel_helpershelper_sheets.xlsx'
Sub parList_f_UpdateParameterOwnerOwner()
    
    Dim parCol As String
    Dim defCol As String
    Dim newValSheetName As String
    Dim parListSheetName As String
    Dim parListDefCol As String
    Dim oldDefValCol As String
    Dim destionationColumnForOldDef As String
    Dim parDescriptionCol As String
    Dim parDestinationColumnForParDesc As String
    Dim parDescCopyBool As String
    Dim startRowNumb
    Dim endRowNumb
    Dim ParListTableStartColumnAndEndColum As String
    

    'finds the last row in the parameter list
    'are used for updateing the parameter owners
    Dim lastRowInParList As String
    Sheets("ParameterList").Select
    lastRowInParList = HelperModule.LastRowInOneColumn("a")
    Sheets("ParameterOwnerUpdater").Select
    Range("B13") = "A7:A" & lastRowInParList
    
        '---------------------------'
        '(1) enable the below functionaly if you want to trigger a input box where the name of the newDef sheet should be written and then we are swithcing to that sheet'
        '---------------------------'
    'inputbox for ensuring the user is selecting the right sheet to begin with, in order to exceute the function'
    'Dim prompt
    'promt = ""
    'promt = InputBox("Enter the sheet name for the sheet containing the new values")
    'Debug.Print "SheetName is set to:" & promt
    'Sheets(promt).Select
        '---------------------------'
        '/(1)'
        '---------------------------'
    
    parListDefCol = Cells(5, "B").Value
    parCol = Cells(3, "B").Value
    defCol = Cells(4, "B").Value
    newValSheetName = Cells(1, "B").Value
    parListSheetName = Cells(2, "B").Value
    startRowNumb = Cells(6, "B").Value
    endRowNumb = Cells(7, "B").Value
    oldDefValCol = Cells(8, "B").Value
    destionationColumnForOldDef = Cells(9, "B").Value
    parDescriptionCol = Cells(10, "B").Value
    parDestinationColumnForParDesc = Cells(11, "B").Value
    parDescCopyBool = Cells(12, "B").Value
    ParListTableStartColumnAndEndColum = Cells(13, "B").Value
    
    'Debug.Print "parListRange Star End " + ParListTableStartColumnAndEndColum


    Dim I As Integer
    
    'Start loop'
    'Debug.Print "Start For Loop"
    
    Dim parNotFoundStringList As String
    'set loop to run a certain length'
    For I = startRowNumb To endRowNumb

    
    Dim tempParCellNumber As String
    Dim tempDefCellNumber As String
    Dim tempOldDefCellNumber As String
    'set the cell number for the current parameter'
    tempParCellNumber = parCol + CStr(I)
    'set the cell number for the current parameters new default value'
    tempDefCellNumber = defCol + CStr(I)
    'set the cell number for the current parameters old default value destionation'
    tempOldDefCellNumber = oldDefValCol + CStr(I)
    
    
    
    'go to new value sheet'
    Sheets(newValSheetName).Select
    'Select first cell in parameter range'
    Range(tempParCellNumber).Select
    'copy par numb and new def'
    Dim tmpParNumb As String
    'extract the current parameter number'
    tmpParNumb = CStr(Range(tempParCellNumber).Value)
    'Debug.Print "tmpParNumb " + tmpParNumb
    Dim tmpParVal As String
    'extract the current parameters new default value'
    tmpParVal = CStr(Range(tempDefCellNumber).Value)
    'Debug.Print "tmpParVal " + tmpParVal
    'Copy the parValue'
    'Application.CutCopyMode = False'
    'Selection.Copy'
   'MsgBox Selection.Value'

    'go to parameter list'
    Sheets(parListSheetName).Select
    Dim tempSelVal As String
    'set temp variable to the copied value'
    tempSelVal = tmp
    'Search for the copied parameter number'
    Dim rgFound As Range
    'remember to update the range to the number the parameter table ends at - else it can't find the parameter to update in the parameter list.'
    'Set rgFound = Range("A7:A5718").Find(tmpParNumb)
    Set rgFound = Range(ParListTableStartColumnAndEndColum).Find(tmpParNumb)

    If rgFound Is Nothing Then
    'enable below msg box to get shown each individual par numb which not are found - else they will be added to the parUpdatersheet in the end.
    'MsgBox "Par" & tmpParNumb & " was not found."
    parNotFoundStringList = parNotFoundStringList & tmpParNumb & ","
    Else
    
    Range(rgFound.Address).Select
    
    'MsgBox "Par found in :" & rgFound.Address''
    Dim formattedValue As String
    formattedValue = tmpParVal
    formattedValue = Replace(tmpParVal, ",", ".")
    'Debug.Print formattedValue
    'Copy old default value'
    Dim oldDefValCopy As String
    oldDefValCopy = Cells(rgFound.Row, oldDefValCol).Value
    'Debug.Print "Old default value copuied = " + oldDefValCopy
    Dim parDescriptionCopy As String
    
         'If we dont have the par desc in the new defaults sheet, we want to copy from the parameter list'
         If parDescCopyBool = "yes" Then parDescriptionCopy = Cells(rgFound.Row, parDescriptionCol).Value
         'Debug.Print "||| parDescription copy: " + parDescriptionCopy
    'Update default value'
    Range(parListDefCol & rgFound.Row).Value = formattedValue
    'Go back to sheet containing the new defaults'
    Sheets(newValSheetName).Select
    Range(destionationColumnForOldDef & I).Value = oldDefValCopy
    'Add old default value for backup'
    
        'Insert the parameter description in the new defaults sheets if we have said yes to that'
        If parDescCopyBool = "yes" Then Range(parDestinationColumnForParDesc & I).Value = parDescriptionCopy
    
   ' MsgBox tmpParVal'
    End If
    
  
    Next I
    
    Sheets(newValSheetName).Select
      
     'if anything have been added to the list, we know we have missing parameters and therefore we wanna perform some actions
       If Len(parNotFoundStringList) > 0 Then
        'remove last comma in the parNotFoundStringList for nicer output formatting
            parNotFoundStringList = Left(parNotFoundStringList, Len(parNotFoundStringList) - 1)
            Range("B14").Value = "" & parNotFoundStringList
       'if we have our if statement above run, we know we dont have any missing parameters, and therefore we run the below
        Else
            Range("B14").Value = "All parameters were found"
            
        End If

    

End Sub

Sub parList_q_Add_new_columns_to_standard_parameterlist()
'
'

'
    Columns("D:D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("D7").Select
    ActiveCell.FormulaR1C1 = "New Min"
    Columns("F:F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F7").Select
    ActiveCell.FormulaR1C1 = "New Max"
    Columns("H:H").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("H7").Select
    ActiveCell.FormulaR1C1 = "New Def"
    Range("L7").Select
    ActiveCell.FormulaR1C1 = "Requestor"
    Range("M7").Select
    ActiveCell.FormulaR1C1 = "Comment"
    Range("N7").Select
    ActiveCell.FormulaR1C1 = "SWSAR"
    Range("L8").Select
End Sub

Sub parList_qq_format_parameterList()
    Call HelperModule.format_range_as_table("A7:H" & HelperModule.LastRowInOneColumn("a"), 1)
End Sub


'resets filters on parameter list
'goes to old par list and copies headers
Sub parList_qqq_CopyHeadersForRegularParList()
'
'RUN THIS AFTER YOU CREATED THE SLC VERSION OF THE PAR LIST
'

'
    Dim parListSheetName As String
    parListSheetName = "ParameterList"
    'Go to the parameterlist sheets
    Sheets(parListSheetName).Select
    'resete filter, by selecting the owner column filter and finding the tablename on the sheet
    ActiveSheet.ListObjects(HelperModule.Get_Table_Name_From_Sheet(parListSheetName)).Range.AutoFilter Field:=11
    'open old parameterlist and go to the parameterlist sheet in the file
    Workbooks.Open Filename:= _
        "https://siemensgamesa.sharepoint.com/teams/OG00570/Product/TurbineParameters/TypeCertificate/01_Platforms/09_NG11/M3.3b/SG-14-222-ParameterList-M3.3b-00519-110000086743-00.xlsx"
    Sheets("ParameterList").Select
    'select the first 6 rows in the file and copy it to the new parmaeterlist file we are generating
    Rows("1:6").Select
    Selection.Copy
    Windows(ThisWorkbook.Name).Activate
    Range("A1").Select
    ActiveSheet.Paste
    'autofit the columns so the copied text is entirely visble
    Range("B1:E1").Select
    Columns("B:B").EntireColumn.AutoFit
    Range("B2").Select
    'go back to the old paramter list and close the file
    Windows("SG-14-222-ParameterList-M3.3b-00519-110000086743-00.xlsx").Activate
    ActiveWindow.Close
End Sub

Sub parList_qqqq_make_input_fields_orange()
    Sheets("ParameterList").Select
    Call HelperModule.style_selection_as_input("7", HelperModule.LastRowInOneColumn("a"), "D, F, H, L, M, N", "Input")
End Sub

Sub parList_xxx_callHelperFuntions()

'Call HelperModule.get_data_from_range("TypeSelectionCompare", "C2:H2")
'format parameter list
'Call HelperModule.format_range_as_table("A7:H" & HelperModule.LastRowInOneColumn("a"), 1)
End Sub

