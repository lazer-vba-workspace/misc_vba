Attribute VB_Name = "slc_parameterlist"
'filteres the parameter list to only show SLC parameters
'copies it to a new sheet
'adding all needed columns for the sheet
Sub SLC_a_Initial_setup_of_slc_par_list()
    
    'Go to the parameter list
    Sheets("ParameterList").Select
    'Filter the parameter list table so we only have SLC relevant parameters
    Call HelperModule.filter_table_by_table_name(HelperModule.Get_Table_Name_From_Sheet("ParameterList"), _
         HelperModule.string_to_Arrary("systemlevelcontrol"), 11)
    
    'find the range for the parameterlist after we filtered
    Dim rng As String
    rng = "A7:K" & HelperModule.LastRowInOneColumn("A")
    'copy the rng to a new sheet
    Call HelperModule.copy_range_to_destination(rng, "ParameterList", "ParameterList_SLC", True, "A13")
    'format the table
    Call HelperModule.format_range_as_table(Selection.Address, 1)
    
    Range("L13") = "Group"
    Range("M13") = "Sub Group"
    Range("N13") = "Obsolete"
    Range("O13") = "Par Exist in CoPaD"
    Range("P13") = "Reviewer"
    Range("Q13") = "Comment"
    Range("R13") = "Lars create SWSAR?"
    
End Sub


'Function for copying all SLC group/sub group info when creating SLC parameter lists
'ensure the sheetname of the SLC input par list have all the right columns, example can be found in the "helper_sheets"
Sub SLC_b_Add_SLC_Groups_to_parameter_list()
'
' AddGroups Macro
'

'
    'Copy slc group/sub grups sheet to current file
    Call OpenFileAndCopy
   
    
    Dim startRow As Integer
    Dim endRow As Integer
    Dim SLC_parameter_group_sheet_name As String
    Dim SLC_parameter_list_sheet_name As String


    'set sheet name, these might need to be updaeted
    SLC_parameter_group_sheet_name = "SLC_Parameter_Groups"
    SLC_parameter_list_sheet_name = "ParameterList_SLC"
    
    'Go to SLC group sheet and find end row numb
    Sheets(SLC_parameter_group_sheet_name).Select
    startRow = 4
    endRow = HelperModule.LastRowInOneColumn("a")
    
   
    
    Dim parList_group_column As String
    Dim parList_subgroup_column
    Dim parList_note_column As String
    Dim parList_column_array(3) As String
    'set which destination columns for group, subgroup, obsolete and Par Exist in CoPaD
    parList_column_array(0) = "L"
    parList_column_array(1) = "M"
    parList_column_array(2) = "N"
    parList_column_array(3) = "O"
    
    'go back to the parmeter list and add details
    Sheets(SLC_parameter_list_sheet_name).Select
    
    'arrary for holding current par numb ,group, subtgroup and obsolete information
    Dim tempVariables(4) As String
    'variable for holding the row numb of the current par we are working with
    Dim tempActiveCellRow As String

    For I = startRow To endRow
        'get par numb
        tempVariables(0) = Worksheets(SLC_parameter_group_sheet_name).Range("A" & I)
        'get group name
        tempVariables(1) = Worksheets(SLC_parameter_group_sheet_name).Range("F" & I)
        'get sub group name
        tempVariables(2) = Worksheets(SLC_parameter_group_sheet_name).Range("G" & I)
        'get obsolete
        tempVariables(3) = Worksheets(SLC_parameter_group_sheet_name).Range("K" & I)
        'get CoPaD info
        tempVariables(4) = Worksheets(SLC_parameter_group_sheet_name).Range("L" & I)
        'Debug.Print tempVariables(0) & " " & tempVariables(1) & " " & tempVariables(2) & g" " & tempVariables(3)
        'Debug.Print "Next i"
        
        'On Error... means if the par is not found we skip the current number and goes to the next one
        On Error Resume Next
        'find the current par from the slc group sheet, and select the cell
        Cells.Find(tempVariables(0), After:=ActiveCell, LookIn:=xlFormulas, LookAt _
        :=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, MatchCase:= _
        False, SearchFormat:=False).Activate
        
        'get row number for active cell
        tempActiveCellRow = ActiveCell.Row
        'add/insert group, subgroup, obsolete and Par exist in CoPaD copied from the SLC par group sheet
        Range(parList_column_array(0) & tempActiveCellRow) = tempVariables(1)
        Range(parList_column_array(1) & tempActiveCellRow) = tempVariables(2)
        Range(parList_column_array(2) & tempActiveCellRow) = tempVariables(3)
        Range(parList_column_array(3) & tempActiveCellRow) = tempVariables(4)
        
        'reset all temp variables for next i - to minimze change of error if any parameters are removed or whatever
        tempVariables(0) = ""
        tempVariables(1) = ""
        tempVariables(2) = ""
        tempVariables(3) = ""
        tempVariables(4) = ""
        tempActiveCellRow = ""
    Next I
    
    
    
End Sub


'copies the first 12 rows of the last SLC parameter list to the new one
Sub SLC_c_copy_headlines_from_old_sheet()

    Workbooks.Open Filename:= _
        "https://siemensgamesa-my.sharepoint.com/personal/lars_mathiesen_siemensgamesa_com/Documents/temp/12_excel_helpers/helper_sheets.xlsx" _
        , UpdateLinks:=0
    Sheets("ParmeterList_SLC").Select
    Rows("1:12").Select
    Selection.Copy
    Windows(ThisWorkbook.Name).Activate
    Range("A1").Select
    ActiveSheet.Paste
    Rows("9:11").Select
    Selection.RowHeight = 24.75
    Cells.Select
    Cells.EntireColumn.AutoFit
    Range("J3").Select
    Windows("helper_sheets.xlsx").Activate
    ActiveWindow.Close
End Sub

Sub SLC_d_make_input_fields_orange()
    Sheets("ParameterList_SLC").Select
    Call HelperModule.style_selection_as_input("13", HelperModule.LastRowInOneColumn("a"), "D, F, H, N, P, Q, R", "Input")
End Sub



'Open file "slc group sheet" and copy the complete overview to the current file and closes the slc group sheet again
Function OpenFileAndCopy()
    Workbooks.Open Filename:= _
           "https://siemensgamesa.sharepoint.com/teams/OG00570/teamslib/SystemLevelControl/ConfigurationManagement/Parameter%20Management/07_Ownership/SLCparameters_overview_00.xlsm"
       Sheets("SLC_Parameter_Groups").Select
       Sheets("SLC_Parameter_Groups").Copy Before:=Workbooks( _
           ThisWorkbook.Name).Sheets(1)
       Windows("SLCparameters_overview_00.xlsm").Activate
       ActiveWindow.Close
End Function




