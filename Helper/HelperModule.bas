Attribute VB_Name = "HelperModule"
'gets data from a range etc like: a1:a5 or a2:r2
'currently the functions only prints out the data and which cell the data comes from
'the indention is that this functions can be used to copy larger amunt of data instead of going through stuff individually.
Function get_data_from_range(sheet_name As String, rng As String)


    Dim range_data_items As Variant
    
    'load data from range into a array
    'etc "TypeSelectionCompare"
    range_data_items = Worksheets(sheet_name).Range(rng)
    
    
    Dim cell_row_counter As Integer
    Dim counter As Integer
    
    'set our start cell number, used for keeping track of which cell numb a given item comes frrom
    cell_row_counter = 2
    counter = 0
    
    Dim item As Variant
    
    'loop through array and print out items and cell numbers
    For Each item In range_data_items
        'some logic needs to be applied here since we just print out the result
        Debug.Print item & " Cell: C" & cell_row_counter + counter
        counter = counter + 1
    Next item


End Sub


'loop through range and find cells with values and print cell value and cell range
'etc B2:B45
'could be extended with including sheetname in the method
Function Find_cells_with_value_in_range(rng As String, sheetname As String)
    Dim myRng
    'myRngString = InputBox("Assign range etc. A1:A25", "Set Range", "A1:A5")
    'etc "B6:B31"
    myRng = rng
    
    'etc TypeSelections
    Sheets(sheetname).Select
    
    Dim dataArrary As Variant
    'load entire range into a arrary
    dataArrary = Range(myRngString)
    
    Dim item As Variant
    
    Dim cell_row_counter As Integer
    'set our starting cell number, for keeping track of what cell number the current item comes from
    cell_row_counter = 6
    'loop through the data
    For Each item In dataArrary
        'if the item's length is bigger that 0 we know we havee data in the given cell
        If Len(item) > 0 Then
            'some logic need to be applied here - since we just are print out
            Debug.Print item & " Cell: B" & cell_row_counter
        'else we kow we have no data "empty" in the cell
        ElseIf Len(item) <= 0 Then
            'some logic need to be applied here - since we just are print out
            Debug.Print "Empty " & " B" & cell_row_counter
        End If
        
        'add 1 for everey time the loop runs since we want to keep track of the cell numbers our items belongs to
        cell_row_counter = cell_row_counter + 1
    'Stop'
    Next item
End Function



'get last row from a column
'could potentioally be extended with including a sheetname as well
Public Function LastRowInOneColumn(column_letter As String) As String
'Find the last used row in a Column: column A in this example
    Dim LastRow As Long
    With ActiveSheet
        LastRow = .Cells(.Rows.Count, column_letter).End(xlUp).Row
    End With
    LastRowInOneColumn = LastRow
End Function



'format table based on range
'table style = 1 for regualr parameter list
'table style = 2 for alarm verison
Function format_range_as_table(rng As String, tableStyleNumb As Integer)
    Dim tableName As String
    Dim tableStyleName As String
    
    If tableStyleNumb = 1 Then
        tableStyleName = "TableStyleMedium2"
    ElseIf tableStyleNumb = 2 Then
        tableStyleName = "TableStyleLight16"
    Else
        tableStyleName = "TableStyleMedium2"
    End If
    
    'set tablename with random number
    tableName = "Table" & vba_random_number(5000, 1)
    
    'apply table formating on speciifed range
    ActiveSheet.ListObjects.Add(xlSrcRange, Range(rng), , xlYes).Name = _
        tableName
    Range(tableName & "[#All]").Select
    ActiveSheet.ListObjects(tableName).TableStyle = tableStyleName

End Function

'generate random number bewteen
Function vba_random_number(upperbound, lowerbound As Integer)
    Dim myRnd As Integer
    myRnd = Int((upperbound - lowerbound + 1) * Rnd + lowerbound)
    vba_random_number = myRnd
End Function

'copy range to a destination
'if add_new_sheet = true , a new sheet will be added and renamed to sheet_name
'if add_new_sheet = false the function will selecet the sheet named "sheet_name"
Function copy_range_to_destination(rng As String, sheet_name_to_copy_from As String, dest_sheet_name As String, add_new_sheet As Boolean, dest_cell As String)
    
    'select sheet to copy data from
    Sheets(sheet_name_to_copy_from).Select
    'select the range we want to copy
    Range(rng).Select
    'copy
    Selection.Copy
    
    If add_new_sheet = True Then
        'add new sheet
        Sheets.Add After:=ActiveSheet
        'selecet range
        Range(dest_cell).Select
        'paste values only
        Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
            :=False, Transpose:=False
        Sheets(ActiveSheet.Name).Name = dest_sheet_name
    ElseIf add_new_sheet = False Then
        'select sheet
        Sheets(dest_sheet_name).Select
        'select range
        Range(dest_cell).Select
        'paste values only
        Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
            :=False, Transpose:=False
    End If

End Function

'get list of all tables in the workbook and their corrosponding sheetname
Function ListTables()
    Dim xTable As ListObject
    Dim xSheet As Worksheet
    Dim i As Long
    i = -1
    Sheets.Add.Name = "Table Name"
    Range("A1") = "Table name"
    Range("B1") = "Sheet name"
    For Each xSheet In Worksheets
        'Debug.Print xSheet.Name
        For Each xTable In xSheet.ListObjects
            i = i + 1
            Sheets("Table Name").Range("A2").Offset(i).Value = xTable.Name
            Sheets("Table Name").Range("B2").Offset(i).Value = xSheet.Name
        Next xTable
    Next
End Function

'returns the name of the table on the given sheet only works if there is one table one the sheet
'else you need to modify the function
Function Get_Table_Name_From_Sheet(sheet_name As String)
    Dim xTable As ListObject
    Dim tableName As String
    For Each xTable In Worksheets(sheet_name).ListObjects
        Debug.Print xTable.Name
        tableName = xTable.Name
    Next
    Get_Table_Name_From_Sheet = tableName
End Function


'function who can take a range like A1:A10 and filter a table within range etc "A7:H300"
'inspiration can be found in the lasttypeselections function
'etc myFilters = Array("ConverterControl", "ElectricalSystems", "GeneratorControl", "Grid", "SystemLevelControl")
Function filter_table_by_range(tableRange As String, myFilters As Variant, filter_field_number As Integer)
    ActiveSheet.Range(tableRange).AutoFilter Field:=filter_field_number, Criteria1:=myFilters, Operator:=xlFilterValues
End Function



'Set filters by tableName
'You can get a tablename for a given sheet by using: HelperModule.Get_Table_Name_From_Sheet("ParameterList")
'etc myFilters = Array("ConverterControl", "ElectricalSystems", "GeneratorControl", "Grid", "SystemLevelControl")
'or myFilters = HelperModule.string_to_Array("TurbineControl, SystemLevelControl, MainBearing")
Function filter_table_by_table_name(tableName As String, myFilters As Variant, filter_field_number As Integer)
    ActiveSheet.ListObjects(tableName).Range.AutoFilter Field:=filter_field_number, Criteria1:=myFilters, Operator:=xlFilterValues
End Function


'remember to seperate with ", " for each arrary element
'etc: systemlevelcontrol, turbinecontrol, bladebearing
'string_to_Arrary("systemlevelcontrol, turbinecontrol, bladebearing") will create a arraru with 3 elements
Function string_to_Arrary(myString As String)
    Dim myArrary As Variant
    'there will be added a new element every time we write ", " after a string to the arrary
    myArrary = Split(myString, ", ")
    'For Each x In myArrary
        'Debug.Print x
    'Next
    string_to_Arrary = myArrary
End Function


'takes a range and turns it into an arrary
'Like range_to_Array("A7:A65")
Function range_to_Array(rng As String)
    Dim myArrary As Variant
    'add the values from the range to the arrary
    myArrary = Range(rng)
    'For Each x In myArrary
    '    Debug.Print x
    'Next
    range_to_Array = myArrary
End Function

'can take values from cells in a given range from another sheet without having to go to the given sheet and puts it into an arrary
Function range_to_Arrary_from_another_sheet(sheetname As String, rng As String)
    Dim myArrary As Variant
    'add the values from the other sheets range to the arrary
    myArrary = Worksheets(sheetname).Range(rng)
    'For Each x In myArrary
        'Debug.Print x
    'Next
    range_to_Arrary_from_another_sheet = myArrary

End Function

Function replace_word_with_other(sheetname As String, searchWord As String, replaceWord As String)
'
    Sheets(sheetname).Select
    Cells.Replace What:=searchWord, Replacement:=replaceWord, _
        LookAt:=xlPart, SearchOrder:=xlByRows, MatchCase:=False, SearchFormat:= _
        False, ReplaceFormat:=False
End Function

'
'inputStyle = "Input" for orange
'inputStyle = "Normal" for orange
'"Bbad" = red, "Good" = green "Neutral" = yellow
'style_selection_as_input("7", LastRowInOneColumn("a"), "D, F, H, L, M, N", "Input")
'functions who takes a range and add a style to it
Function style_selection_as_input(startRow, endRow As String, columns_to_format As String, inputStyle As String)
    'slc
    'L, f, h, n, p, q, r,
    
    'regular
    'D, F, H, L, M, N
    Dim myColumns As Variant
    myColumns = HelperModule.string_to_Arrary(columns_to_format)
    
    For Each col In myColumns
        Range(col & startRow & ":" & col & endRow).Select
        Selection.Style = inputStyle
    Next
    'Range(rng).Select
    'Selection.Style = "input"
End Function

'takes a a column letter as input and return the column number
Function column_letter_to_number(col_letter As String)
    column_letter_to_number = Cells(1, col_letter).Column
End Function

Function get_length_of_arrary(myArrary As Variant)
    get_length_of_arrary = UBound(myArrary) - LBound(myArrary) + 1
End Function

Function Col_numb_to_letter(lngCol As Long) As String
    Dim vArr
    vArr = Split(Cells(1, lngCol).Address(True, False), "$")
    Col_numb_to_letter = vArr(0)
End Function

'wrap text in range
Function WrapText_in_range(myRng As String)
    Range(myRng).Select
    With Selection
        .VerticalAlignment = xlBottom
        .WrapText = True
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
End Function

Function Change_column_width(sheetname As String, newWidth As String, affectedColumn As String)
    With Worksheets(sheetname).Columns(affectedColumn)
     .ColumnWidth = newWidth
    End With
End Function

Sub helper_module_Fire()
    'Call vba_random_number
    'Call replace_word_with_other("parameterlist", "WTGType:DD_14000_222_00_N0_00_00", "WTGType:DD_NG12_xxx_00_N0_00_00")
    'Call replace_word_with_other("parameterlist", "HWProfile:SD10003", "HWProfile:SD10004")
    'Call replace_word_with_other("parameterlist", "BldType:B108_00", "BldType:B_NB18_00")
    'Call replace_word_with_other("parameterlist", "GenType:PmgDD11_01", "GenType:PmgDD12_01")
    'Debug.Print vba_random_number(5000, 1)
    'Call style_selection_as_input("7", LastRowInOneColumn("a"), "D, F, H, L, M, N", "Input")
    'Call style_selection_as_input("13", LastRowInOneColumn("a"), "D, F, H, N, P, Q, R", "Input")
End Sub


