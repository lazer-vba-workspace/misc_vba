Attribute VB_Name = "ParameterOwners"


'copies the latest parameter owner sheet to this file
Sub Copy_Parameter_Owners_Sheet()
Attribute Copy_Parameter_Owners_Sheet.VB_ProcData.VB_Invoke_Func = " \n14"
'
' Macro1 Macro
'

'
    Workbooks.Open Filename:= _
        "https://siemensgamesa.sharepoint.com/teams/OG00570/Product/TurbineParameters/Parameter_Ownership.xlsx"
    Sheets("Parameter owners").Select
    Sheets("Parameter owners").Copy After:=Workbooks( _
        ThisWorkbook.Name).Sheets(2)
    Windows("Parameter_Ownership.xlsx").Activate
    ActiveWindow.Close
End Sub

'Get all unique parameter groups to its own seperate sheet
Sub Get_List_Of_Owner_Groups()
    
    Dim Parameter_Owner_Sheet_Name As String
    Parameter_Owner_Sheet_Name = "Parameter owners"
    Dim Owner_Groups_sheet_name As String
    Owner_Groups_sheet_name = "Owner groups"
    
    Dim rng As String
    rng = "A2:A" & HelperModule.LastRowInOneColumn("A")
    
    Sheets(Parameter_Owner_Sheet_Name).Select
    Range(rng).Select
    Selection.Copy
    Sheets.Add After:=ActiveSheet
    Range("A2").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Sheets(ActiveSheet.Name).Name = "Owner_Groups"
    ActiveSheet.Range(rng).RemoveDuplicates Columns:=1, Header:=xlNo
    
    Range("A1") = "Owner Groups"
End Sub

'THIS IS THE FUNCTION WE ARE WORKING AT
Sub extractownersForAGivenPlatforToNewSheet()
'
'
    Dim owners_sheet_name As String
    owners_sheet_name = "owners from request"
    Dim filter_field_numb As Integer
    
    Call Select_first_cell_in_sheet_by_search_word("ng11")
    filter_field_numb = ActiveCell.Column
    
    Dim last_row_numb As Long
    last_row_numb = HelperModule.LastRowInOneColumn("A")
    
    Sheets("Parameter owners").Select
    ActiveSheet.ListObjects(HelperModule.Get_Table_Name_From_Sheet("Parameter owners")).Range.AutoFilter Field:=filter_field_numb, Criteria1:= _
        "<>"
    Range("A2:A" & last_row_numb).Select
    Selection.Copy
    Sheets.Add After:=ActiveSheet
    Range("A2").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Sheets(ActiveSheet.Name).Name = owners_sheet_name
    Sheets("Parameter owners").Select
    Range("C2:C" & last_row_numb).Select
    Application.CutCopyMode = False
    Selection.Copy
    Sheets(owners_sheet_name).Select
    Range("B2").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Sheets("Parameter owners").Select
End Sub


Function Select_first_cell_in_sheet_by_search_word(keyword As String)
    Cells.Find(What:=keyword, After:=ActiveCell, LookIn:=xlFormulas, LookAt _
        :=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, MatchCase:= _
        False, SearchFormat:=False).Activate
End Function

Sub check()
    Sheets("owners from request").Select
    Dim last_row_numb As Long
    last_row_numb = HelperModule.LastRowInOneColumn("A")
    Dim ownersGroupsToCheck As Variant
    ownersGroupsToCheck = HelperModule.range_to_Array("A2:A" & last_row_numb)
    
    Dim officalOwnerGroups As Variant
    Dim officalOwner_rng As String
    officalOwner_rng = "A2:A" & HelperModule.LastRowInOneColumn("a")
    officalOwnerGroups = HelperModule.range_to_Array(officalOwner_rng)
    Sheets("Owner_Groups").Select
    'Sheets("owners from request").Select
    
 
End Sub
